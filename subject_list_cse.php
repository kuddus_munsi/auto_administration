<?php 
    include "source/header.php" ;
    include "source/sidebar.php" ;
 

    
    $subject = new Subjects();
    $msg = Message::getMessage();

    if(isset($_GET['subject_id_for_delete'])){
        $subject_id = $_GET['subject_id_for_delete'];
        $delete = $subject->deleteSubject($subject_id);
    }
?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
      List of All Subject 
      <h2 class="text-center text-success"><?php echo "<div id='message'> $msg</div>"?> </h2>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">subject</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">          
      <div class="box">
            <div class="box-body">
              <h2 class="text-danger">1st Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showFirstSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $subjects_1st){

                          ?>
                    <tr>
                        <td><?php echo $subjects_1st['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_1st['subject_name'] ; ?></td>
                        <td><?php echo $subjects_1st['subject_code'] ; ?></td>
                        <td><?php echo $subjects_1st['subject_type'] ; ?></td>
                        <td><?php echo $subjects_1st['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_1st['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_1st['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_1st['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>

          <div class="box">
            <div class="box-body">
              <h2 class="text-danger">2nd Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showSecondSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $serial=>$subjects_2nd){?>
                    <tr>
                        <td><?php echo $subjects_2nd['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_2nd['subject_name'] ; ?></td>
                        <td><?php echo $subjects_2nd['subject_code'] ; ?></td>
                        <td><?php echo $subjects_2nd['subject_type'] ; ?></td>
                        <td><?php echo $subjects_2nd['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_2nd['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_2nd['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_2nd['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>

                    <div class="box">
            <div class="box-body">
              <h2 class="text-danger">3rd Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showThirdSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $serial=>$subjects_3rd){?>
                    <tr>
                        <td><?php echo $subjects_3rd['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_3rd['subject_name'] ; ?></td>
                        <td><?php echo $subjects_3rd['subject_code'] ; ?></td>
                        <td><?php echo $subjects_3rd['subject_type'] ; ?></td>
                        <td><?php echo $subjects_3rd['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_3rd['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_3rd['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_3rd['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
                    <div class="box">
            <div class="box-body">
              <h2 class="text-danger">4th Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showFourthSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $serial=>$subjects_4th){?>
                    <tr>
                        <td><?php echo $subjects_4th['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_4th['subject_name'] ; ?></td>
                        <td><?php echo $subjects_4th['subject_code'] ; ?></td>
                        <td><?php echo $subjects_4th['subject_type'] ; ?></td>
                        <td><?php echo $subjects_4th['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_4th['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_4th['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_4th['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
                    <div class="box">
            <div class="box-body">
              <h2 class="text-danger">5th Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showFifthSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $serial=>$subjects_5th){?>
                    <tr>
                        <td><?php echo $subjects_5th['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_5th['subject_name'] ; ?></td>
                        <td><?php echo $subjects_5th['subject_code'] ; ?></td>
                        <td><?php echo $subjects_5th['subject_type'] ; ?></td>
                        <td><?php echo $subjects_5th['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_5th['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_5th['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_5th['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
                    <div class="box">
            <div class="box-body">
              <h2 class="text-danger">6th Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showSixSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $serial=>$subjects_6th){?>
                    <tr>
                        <td><?php echo $subjects_6th['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_6th['subject_name'] ; ?></td>
                        <td><?php echo $subjects_6th['subject_code'] ; ?></td>
                        <td><?php echo $subjects_6th['subject_type'] ; ?></td>
                        <td><?php echo $subjects_6th['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_6th['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_6th['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_6th['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
                    <div class="box">
            <div class="box-body">
              <h2 class="text-danger">7th Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showSevenSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $serial=>$subjects_7th){?>
                    <tr>
                        <td><?php echo $subjects_7th['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_7th['subject_name'] ; ?></td>
                        <td><?php echo $subjects_7th['subject_code'] ; ?></td>
                        <td><?php echo $subjects_7th['subject_type'] ; ?></td>
                        <td><?php echo $subjects_7th['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_7th['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_7th['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_7th['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
                    <div class="box">
            <div class="box-body">
              <h2 class="text-danger">8th Semester Subjects</h2>
              <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_subject = $subject->showEightSemesterSubjectsForCSE();
                    if($show_subject){
                        foreach($show_subject as $serial=>$subjects_8th){?>
                    <tr>
                        <td><?php echo $subjects_8th['subject_in_semester'] ; ?></td>
                        <td><?php echo $subjects_8th['subject_name'] ; ?></td>
                        <td><?php echo $subjects_8th['subject_code'] ; ?></td>
                        <td><?php echo $subjects_8th['subject_type'] ; ?></td>
                        <td><?php echo $subjects_8th['subject_credit'] ; ?></td>
                        <td><span style="color: green"><?php echo $subjects_8th['live_status'] ; ?></span> <span style="color: red;">to Now</span></td>
                        <td>
                          <a href="subject_edit_cse.php?subject_id_for_edit=<?php echo $subjects_8th['id'];?>">Edit</a> ||
                          <a onclick="return confirm('Are you sure to delete');" href="?subject_id_for_delete=<?php echo $subjects_8th['id'];?>">Delete</a> 

                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Semester</th>
                  <th>Subject Name</th>
                  <th>Subject Code</th>
                  <th>Subject Type</th>
                  <th>Credit</th> 
                  <th>Live</th> 
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>

        </div>
   </section>
<?php include "source/footer.php" ; ?>