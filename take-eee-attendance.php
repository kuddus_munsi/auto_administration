<?php 
    include "source/header.php" ;
    include "source/sidebar.php" ;
?>


  <div class="content-wrapper">
    <section class="content-header">
      <h1>
      Attendance Sheet of EEE
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">attendance</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
          
      <div class="box">
          <div class="box-body">
            <div class="col-md-12">
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <h2 class="text-center">Select Semester</h2>
              <div class="form-group">
                <label for="semester_for_eee">Semester</label>
                <select  id="semester_for_eee" class="form-control">
                 <option value="1">1st</option>
                 <option value="2">2nd</option>
                 <option value="3">3rd</option>
                 <option value="4">4th</option>
                 <option value="5">5th</option>
                 <option value="6">6th</option>
                 <option value="7">7th</option>
                 <option value="8">8th</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <h2 class="text-center">Select Subject</h2>
              <div class="form-group">
                <label for="subject_for_eee">Subject</label>
                <select  id="subject_for_eee" class="form-control">
                  <option>select semester first</option>
                </select>
                  
              </div>
            </div>
            <div class="col-md-1">
              <br><br><br>
              <div class="form-group">
                <label for="send_attendance_request_for_eee">Request</label>
                <input type="button" class="form-control btn btn-primary" id="send_attendance_request_for_eee" value="Send">
              </div>
            </div>
             <div class="col-md-1">
              <br><br><br>
              <div class="form-group">
                <label for="close_attendance_request_for_eee">Request</label>
                <input type="button" class="form-control btn btn-primary" id="close_attendance_request_for_eee" value="Close">
              </div>
            </div>
             <div class="col-md-2">
              <br><br><br>
              <div class="form-group">
                <label for="see_attendance_for_eee">Todays</label>
                <input type="button" class="form-control btn btn-primary" id="see_attendance_for_eee" value="Attendance">
              </div>
            </div>
            <div class="col-md-1"></div>
          </div>
          </div>
          </div>
      </div>
   </section>
   <div id="todays_attendance_eee"></div>

   <!-- any day attendance check -->
     <section class="content">
      <div class="row">
          <h1 class="text-center" style="border-right: solid 2px red;border-left: solid 2px red;border-top: solid 2px red;">See Any Attendance</h1>
      <div class="box">
          <div class="box-body">
            <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group">
                <label for="select_eee_department_any_for_teacher">Department</label>
                <select  id="select_eee_department_any_for_teacher" class="form-control">
                 <option value="eee">EEE</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">            
              <div class="form-group">
                <label for="select_eee_semester_any_for_teacher">Semester</label>
                <select  id="select_eee_semester_any_for_teacher" class="form-control">
                 <option value="1">1st</option>
                 <option value="2">2nd</option>
                 <option value="3">3rd</option>
                 <option value="4">4th</option>
                 <option value="5">5th</option>
                 <option value="6">6th</option>
                 <option value="7">7th</option>
                 <option value="8">8th</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="select_eee_subject_any_for_teacher">Subject</label>
                <select  id="select_eee_subject_any_for_teacher" class="form-control">
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="select_eee_date_any_for_teacher">Date</label>
                <input type="date" class="form-control" id="select_eee_date_any_for_teacher" value="Date">
              </div>
            </div>
             <div class="col-md-2">
              <div class="form-group">
                <label for="see_attendance_any_for_teacher">SEE</label>
                <input type="button" class="form-control btn btn-primary" id="see_eee_attendance_any_for_teacher" value="Attendance">
              </div>
            </div>
          </div>
          </div>
          </div>
      </div>
  </section>
   <div id="any_eee__attendance_for_teacher"></div>

   <!--month attendance check -->
     <section class="content">
      <div class="row">
          <h1 class="text-center" style="border-right: solid 2px red;border-left: solid 2px red;border-top: solid 2px red;">Month wise Attendance</h1>
      <div class="box">
          <div class="box-body">
            <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group">
                <label for="select_eee_department_monthly_for_teacher">Department</label>
                <select  id="select_eee_department_monthly_for_teacher" class="form-control">
                 <option value="eee">EEE</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">            
              <div class="form-group">
                <label for="select_eee_semester_monthly_for_teacher">Semester</label>
                <select  id="select_eee_semester_monthly_for_teacher" class="form-control">
                 <option value="1">1st</option>
                 <option value="2">2nd</option>
                 <option value="3">3rd</option>
                 <option value="4">4th</option>
                 <option value="5">5th</option>
                 <option value="6">6th</option>
                 <option value="7">7th</option>
                 <option value="8">8th</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="select_eee_subject_monthly_for_teacher">Subject</label>
                <select  id="select_eee_subject_monthly_for_teacher" class="form-control">
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label for="select_eee_date_from_monthly_for_teacher">From</label>
                <input type="date" class="form-control" id="select_eee_date_from_monthly_for_teacher" value="Date">
              </div>
            </div>
             <div class="col-md-2">
              <div class="form-group">
                <label for="select_eee_date_to_monthly_for_teacher">To</label>
                <input type="date" class="form-control" id="select_eee_date_to_monthly_for_teacher" value="Date">
              </div>
            </div> 
          </div>
          <div class="col-md-12">
          <div class="col-md-5"></div>
            <div class="col-md-2">
              <div class="form-group">
                <input type="button" class="form-control btn btn-primary" id="see_eee_attendance_monthly_for_teacher" value="Attendance">
              </div>
            </div>
          <div class="col-md-5"></div>
          </div>
          </div>
          </div>
      </div>
  </section>
   <div id="monthly_eee_attendance_for_teacher"></div>
<?php include "source/footer.php" ; ?>