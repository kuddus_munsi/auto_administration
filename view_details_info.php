<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;


    $msg = Message::getMessage();
    $student = new Students();

    $student_roll = $_GET['roll'];
    $student_department = $_GET['dept'];

    if($student_roll != "" && $student_department != ""){
      $getStudent_info = $student->profileByIdAndDepartment($student_roll,$student_department);
  }

 ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <h2 class="text-center text-danger"><?php echo "<div id='message'> $msg</div>"?> </h2>
        <small>Student Details</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">details</li>
      </ol>
      
    </section>

    <section class="content">
      <div class="row">
            <div class="col-md-12">
          
                <style>
                    .font-sizeD{
                        font-size:20px;
                    }
                    .font_sizeD{
                        font-size:20px;
                    }
                </style>
                <?php 
                    if(isset($getStudent_info)){
                    foreach($getStudent_info as $view){ ?>
                    
                    <div class="box box-widget widget-user-2">
                   <div class="widget-user-header bg-green" style="margin-bottom: 40px;">
                    <div class="widget-user-image">
                        <img class="img-circle" src="<?php echo $view['std_image'];?>" alt="User Avatar" style="width: 118px;height: 124px; float: left;margin-top: -24px;">
                        <h3 class="widget-user-username"><?php echo strtoupper($view['std_name']);?></h3>
                        <br>
                    </div>
                    </div>
                    <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                            <li class="font-sizeD"><a href="#">Form ID : <span class="pull-right "><?php echo $view['std_roll_no'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Email : <span class="pull-right "><?php echo $view['std_email'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Apply for Department : <span class="pull-right "><?php echo $view['std_dept_name'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Fathers Name : <span class="pull-right "><?php echo $view['std_father_name'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Mothers Name : <span class="pull-right "><?php echo $view['std_mother_name'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Gardian Name : <span class="pull-right "><?php echo $view['std_guardian_name'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Relation with Gargian : <span class="pull-right "><?php echo $view['std_guard_relation'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Prensent Address : <span class="pull-right "><?php echo $view['std_per_address'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Permanent Address : <span class="pull-right "><?php echo $view['std_pre_address'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Date Of Birth :  <span class="pull-right "><?php echo $view['std_dob'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Nationality : <span class="pull-right "><?php echo $view['std_nationality'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Religion : <span class="pull-right "><?php echo $view['std_religion'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Super Visor : <span class="pull-right "><?php echo $view['std_supervisor'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Department Location : <span class="pull-right "><?php echo $view['std_dept_location']."Floor";?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Batch No : <span class="pull-right "><?php echo $view['std_batch_no']."th";?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Session :  <span class="pull-right "><?php echo "<span class='text-success'>".$view['std_session_from']."</span>". "  To  " ."<span class='text-danger'>".$view['std_session_to']."</span>";?></span></a></li>
                        </ul>
                        </div>
                        <hr>
                        <div class="box">
                            <div class="box-body">
                            <table class="table table-bordered">
                            <tr>
                                <th style="width: 10px">Examination</th>
                                <th style="width: 25px">Board</th>
                                <th style="width: 10px">Year</th>
                                <th style="width: 25px">School/College</th>
                                <th style="width: 10px">Divition Grade</th>
                                <th style="width: 10px">CGPA</th>
                                <th style="width: 10px">Subject</th>
                            </tr>
                                <tr class="font_sizeD">
                                <td><?php echo $view['std_ac_ssc_exam']; ?></td>
                                <td><?php echo $view['std_ac_ssc_board']; ?></td>
                                <td><?php echo $view['std_ac_ssc_year']; ?></td>
                                <td><?php echo $view['std_ac_college_ssc_school']; ?></td>
                                <td><?php echo $view['std_ac_div_ssc_grade']; ?></td>
                                <td><?php echo $view['std_ac_ssc_cgpa']; ?></td>
                                <td><?php echo $view['std_ac_ssc_subject']; ?></td>
                                
                            </tr>
                            </tr>
                                <tr class="font_sizeD">
                                <td><?php echo $view['std_ac_hsc_exam']; ?></td>
                                <td><?php echo $view['std_ac_hsc_board']; ?></td>
                                <td><?php echo $view['std_ac_hsc_year']; ?></td>
                                <td><?php echo $view['std_ac_college_hsc_school']; ?></td>
                                <td><?php echo $view['std_ac_div_hsc_grade']; ?></td>
                                <td><?php echo $view['std_ac_hsc_cgpa']; ?></td>
                                <td><?php echo $view['std_ac_hsc_subject']; ?></td>
                                
                            </tr>
                            </table>
                            </div>
                        </div>

                        <div class="box">
                            <div class="box-body">
                            <table class="table table-bordered">
                            <tr>
                                <th style="width: 10px">Subject Name</th>
                                <th style="width: 25px">Subject Code</th>
                                <th style="width: 10px">Subject Type</th>
                                <th style="width: 25px">Subject Status</th>
                            </tr>
                            <?php 
                                $semester = $
                                $getSubject = $subject->getSubjectSemeserWise($semester);
                             ?>
                            <tr class="font_sizeD">
                                <td><?php echo $key+1;;?></td>
                                <td><?php echo $view['subject_name']; ?></td>
                                <td><?php echo $view['subject_code']; ?></td>
                                <td><?php echo $view['subject_type']; ?></td>
                                
                            </tr>
                            </table>
                            </div>
                        </div>
                <?php } } ?>
            </div>
          </div>
        </div>
      </div>
   </section>
<?php include "source/footer.php" ; ?>