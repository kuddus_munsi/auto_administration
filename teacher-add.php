<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;



  $teacher = new Teacher();

  if($_SERVER['REQUEST_METHOD'] == "POST"){
    $teacher_info = $teacher->storeInfo($_POST);
  }

  $msg = Message::getMessage();


 ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
     <h2 class="text-center text-danger"><?php echo "<div id='message'> $msg</div>"?> </h2>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
 <section class="content">
      <div class="row">
         <div class="box box-primary">
          <div class="box-body">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <form role="form" method="post" action="teacher-add.php" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="teacher_name">Teacher Name</label>
                  <input type="text" class="form-control" id="teacher_name" name="teacher_name" placeholder="teacher name write here">
                </div>

                <div class="form-group">
                  <label for="teacher_code">Teacher ID</label>
                  <input type="text" class="form-control" id="teacher_code" name="teacher_code" placeholder="techer code here">
                </div>

                <div class="form-group">
                  <label for="teacher_designation">Teacher Designation</label>
                  <input type="text" class="form-control" id="teacher_designation" name="teacher_designation" placeholder="example, Associate professor">
                </div>


                <div class="form-group">
                  <label for="teacher_department">Teacher Department</label>
                  <select  class="form-control" id="teacher_department" name="teacher_department">
                    <option value="cse">CSE</option>
                    <option value="eee">EEE</option>
                    <option value="ete">ETE</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="teacher_image">Teacher Image</label>
                  <input type="file" class="form-control" id="teacher_image" name="teacher_image">
                </div>


                <div class="form-group">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>

                </div>
              </form>
            </div>
            <div class="col-md-2"></div>
          </div>
        </div>


      </div>
   </section>
<?php include "source/footer.php" ; ?>