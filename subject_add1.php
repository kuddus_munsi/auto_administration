<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;

  $subject = new Subjects();
  $msg = Message::getMessage();

  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['add_subject'])){
    $storeSuject = $subject->storeSubjectInfo($_POST);
  }
 ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Subject Add</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">subject add</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">

      <div class="col-md-2"></div>

        <div class="col-md-8">
           <div class="box box-primary">
                 <div class="box">
                            <div class="box-body">
                            <form action="" role="form" method="post">
                                <div class="col-md-6 pull-left">
                                <div class="form-group">
                                    <label for="subject_name">Subject Name</label>
                                    <input type="text" class="form-control" id="subject_name" name="subject_name" placeholder="enter subject name">
                                </div>
                                <div class="form-group">
                                    <label for="subject_code">Subject Code</label>
                                    <input type="text" class="form-control" id="subject_code" name="subject_code">
                                </div> 
                                <div class="form-group">
                                    <label>Live From:</label>
                                    <div class="input-group date">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="date" name="session_from" class="form-control" id="datepicker">
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6 pull-right">
                                   <div class="form-group">
                                      <label for="subject_type">Subject Type</label>
                                      <input type="text" class="form-control" id="subject_type" name="subject_type">
                                  </div>
                                  <div class="form-group">
                                      <label for="subject_type">Subject Type</label>
                                      <select  class="form-control" id="subject_type" name="subject_type">
                                        <option value="hell">hello</option>
                                      </select>
                                  </div>
                                    <div class="form-group">
                                        <label>Live  To:</label>
                                        <div class="input-group date">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="date" name="session_to" class="form-control" id="datepicker">
                                        </div>
                                    </div>
                                </div>
                                    <br>
                                    <div class="form-group">
                                        <button type="submit"  class="btn btn-primary form-control">make student</button>
                                     </div>
                            </form>
                          </div>
                        </div>
           </div>
        </div>
    <div class="col-md-2"></div>
    </div>
    </section>
<script>
    jQuery(
        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )

</script>
<?php include "source/footer.php" ; ?>