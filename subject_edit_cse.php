<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;

  $subject = new Subjects();
  $msg = Message::getMessage();

    if(isset($_GET['subject_id_for_edit'])){
        $subject_id = $_GET['subject_id_for_edit'];
        $getSubject = $subject->getSubjectByCode($subject_id);
    }

  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['update_subject'])){
    $updateSuject = $subject->updateSubjectInfo_cse($_POST);
  }
 ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Subject Edit</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">subject edit</li>
      </ol>
    </section>
<section class="content">
  <div class="row">
  <div class="col-md-3"></div>
    <div class="col-md-6">
     <div class="box box-primary">
      <h2 class="text-center">Edit SUBJECTS</h2>
      <h2 class="text-center"><a class="btn btn-info" href="subject_list_cse.php">Go Back</a></h2>
      <h2 class="text-center text-danger"><?php echo "<div id='message'> $msg</div>"?> </h2>
           <form role="form" method="post" action="">
              <div class="box-body">
                <?php 

                  if(isset($getSubject)){
                    foreach ($getSubject as $value) { 
                    } } ?>
              <input type="hidden" name="id" value="<?php echo $value['id'];?>">
                <div class="form-group">
                  <label for="subject_code">Subject Code :</label>
                      <input type="text" class="form-control" id="subject_code" name="subject_code" value="<?php echo $value['subject_code'];?>" >
                </div>
                <div class="form-group">
                  <label for="subject_name">Subjects Name :</label>
                      <input type="text" class="form-control" id="subject_name" name="subject_name" value="<?php echo $value['subject_name'];?>" >
                </div>
                <div class="form-group">
                  <label for="subject_type">Subject Type</label>
                  <input type="text" class="form-control" id="subject_type" name="subject_type" value="<?php echo $value['subject_type'];?>" >
                </div>
                <div class="form-group">
                  <label for="subject_credit">Subject Credit</label>
                   <select  class="form-control" id="subject_credit" name="subject_credit">
                    <?php 
                      $getCredit = $subject->getSubjectCredit();
                      if($getCredit){
                        foreach ($getCredit as $data) {?>
                        <option value="<?php echo $data['subject_credit'];?>"
                            <?php if($value['subject_credit'] == $data['subject_credit']) echo "selected"; ?>
                          ><?php echo $data['subject_credit'];?> Credit</option>
                    <?php } } ?>
                   </select>
                </div>
                <div class="form-group">
                  <label for="subject_department">Department</label>
                  <select   class="form-control" id="subject_department" name="subject_department" >
                  <?php 
                    $getdept = $subject->getAllDepartment();
                    if($getdept){
                      foreach ($getdept as $dept) {?>
                       <option value="<?php echo $dept['department_name']; ?>"
                          <?php if($value['subject_department'] == $dept['department_name']) echo "selected"; ?>
                        >
                        <?php echo strtoupper($dept['department_name']); ?></option>
                    <?php } } ?>

                  </select>
                </div>
                <div class="form-group">
                  <label for="subject_in_semester">Semester</label>
                  <select   class="form-control" id="subject_in_semester" name="subject_in_semester" >
                  <?php 
                    $getSemester = $subject->getAllSemester();
                    if($getSemester){
                      foreach ($getSemester as $semester) {?>
                       <option value="<?php echo $semester['semester']; ?>"
                          <?php if($value['subject_in_semester'] == $semester['semester']) echo "selected"; ?>
                        >
                        <?php echo $semester['semester']; ?>th</option>
                    <?php } } ?>

                  </select>
                </div>
                <div class="form-group">
                 <button type="submit" name="update_subject" class="btn btn-primary form-control">Update</button>
                </div>
                </div>
              </form>
</div>
<div class="col-md-3"></div>
</div>
</section>
<?php include "source/footer.php" ; ?>