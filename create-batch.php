<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;
  
  $batch = new Batches();

  if($_SERVER['REQUEST_METHOD'] == "POST"){
    $store_batch = $batch->storBatch($_POST);
  }

  $msg = Message::getMessage();

 ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

<section class="content">
<div class="row">
<div class="col-md-2"></div>
  <div class="col-md-8">
    <div class="box box-widget widget-user-2">
        <div class="widget-user-header bg-yellow">
              <h2 class="widget-user-desc">Create Batch</h2>
              <h2 class="text-center text-success"><?php echo "<div id='message'> $msg</div>"?> </h2>
            </div>
            <div class="box-footer no-padding">  
            <form role="form" method="post" action="">
              <div class="box-body">
                <div class="form-group">
                  <label for="batch_no">Batch Name :</label>
                    <input type="text" class="form-control" id="batch_no" name="batch_no" placeholder="write batch name">
                </div>
                <button type="submit" class="btn btn success"> Submit</button>
                </div>
            </form>
        </div>
    </div>
 </div>
<div class="col-md-2"></div>
</div>
</section>
<?php include "source/footer.php" ; ?>