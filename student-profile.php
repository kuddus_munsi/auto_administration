<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;

  $student = new Students();
  $attendance = new Attendance();

 ?>

 <script type="text/javascript">
   function submit_attendance(roll,dept,batch,semester,subject_code)
   {

 /*   alert(roll);
    alert(dept);
    alert(batch);
    alert(semester);
    alert(subject_code);
    return false;*/
    var roll = roll, 
        dept = dept,
        batch=batch,
        semester=semester,
        subject_code = subject_code;
      $.ajax({
          url:"ajax/submit_attendance_by_id.php",
          method:"POST",
          data:{roll:roll,dept:dept,batch:batch,semester:semester,subject_code:subject_code},
          success:function(data){
              if(data == "success"){
                alert("Attendance Submitted Today");
              }else if (data == "resubmit"){
                alert("Attendance Already Submitted Today");
              }
          }
          
      });
   }

   function see_course(semester)
   {
    var semester = semester;
    $.ajax({
          url:"ajax/show_course_by_semester.php",
          method:"POST",
          data:{semester:semester},
          success:function(data){
              $("#course").html(data);
          }
          
      });
   }


 </script>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">profile</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="box">
       <div class="col-md-12">
         <div class="col-md-2"></div>
         <div class="col-md-8">
          <?php 
              $student_roll =Session::get("student_roll");
              $student_department = Session::get("student_dept");

              if($student_roll != "" && $student_department != ""){
                $getStudent_info = $student->profileByIdAndDepartment($student_roll,$student_department);
              foreach($getStudent_info as $profile){  ?>
              
              <div class="box box-widget widget-user-2">
              <div class="widget-user-header bg-green" style="margin-bottom: 40px;">
              <div class="widget-user-image">
                  <img class="img-circle" src="<?php echo $profile['std_image'];?>" alt="User Avatar" style="width: 118px;height: 124px; float: left;margin-top: -24px;">
                  <h3 class="widget-user-username"><?php echo strtoupper($profile['std_name']);?></h3>
                  <br>
              </div>
              </div>
              <div class="box-footer no-padding">
                      <ul class="nav nav-stacked">
                      <li class="font-sizeD"><a href="#">Form ID : <span class="pull-right "><?php echo $profile['std_roll_no'];?></span></a></li>
                      <li class="font-sizeD" ><a href="#">Email : <span class="pull-right "><?php echo $profile['std_email'];?></span></a></li>
                      <li class="font-sizeD" ><a href="#">Apply for Department : <span class="pull-right "><?php echo $profile['std_dept_name'];?></span></a></li>
                      <li class="font-sizeD" ><a class="pull-right btn btn-primary" href="view_details_info.php?roll=<?php echo $profile['std_roll_no'];?>&&dept=<?php echo $profile['std_dept_name'];?>"> <span class="pull-right ">View Details</span></a></li>
                  </ul>
                  </div>      
               <?php } }else{echo "no";} ?>
            </div>
          </div>
         </div>
         <div class="col-md-2"></div>
       </div>  
       <div class="col-md-12">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <h3 class="text-danger" style="text-decoration: underline;">Click The Following Button To Perform</h3><br>
          <?php 
            $roll = $profile['std_roll_no'];
            $dept = $profile['std_dept_name'];
            $batch_no = $profile['std_batch_no'];
            $semester_no = $student->getSemesterByRoll($roll,$dept);
            $check_attendance_status = $student->checkAttendanceStatus($semester_no,$dept);
            $getCourseId = $attendance->getCourseIdForAttendance($semester_no,$dept);
           ?>
           <button class="btn btn-primary" id="test">See Result</button>
           <button class="btn btn-success">See Attendance</button>
           <button class="btn btn-secondary" onclick="see_course(<?php echo $semester_no?>)">See Course</button>
           <button class="btn btn-info" id="attendance_button"  onclick="submit_attendance('<?php echo $roll?>','<?php echo $dept?>','<?php echo $batch_no?>','<?php echo $semester_no?>','<?php echo $getCourseId?>');"
            >Give Attendance</button>

        
         </div>
        <div class="col-md-2"></div>
       </div>  

        <div class="col-md-12">
          <div class="col-md-2"></div>
          <div class="col-md-8">
           <div id="course"></div>
          </div>
          <div class="col-md-2"></div>
        </div>

</div>
      </div>
   </section>
<?php include "source/footer.php" ; ?>