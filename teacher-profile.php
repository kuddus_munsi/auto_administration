<?php


if(isset($_POST) && !empty($_FILES['image']['name'])){


   $name = $_FILES['image']['name'];
   list($txt, $ext) = explode(".", $name);
   $image_name = time().".".$ext;
   $tmp = $_FILES['image']['tmp_name'];


     if(move_uploaded_file($tmp, 'uploads/'.$image_name)){
        echo "<img width='300px' src='uploads/".$image_name."' class='preview'>";
        echo $_POST['kuddus'];
     }else{
        echo "image uploading failed";
     }
  }else{
     echo "Please select image";
  }


?>

<!DOCTYPE html>
<html>
<head>
    <title>Upload image using ajax in php example</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
    <script src="http://malsup.github.com/jquery.form.js"></script> 
</head>
<body>


<div class="container">
    <h2>PHP - Jquery Ajax Image uploading Example</h2>
    <div class="panel panel-primary">
      <div class="panel-heading">Ajax Image upload jquery php example</div>
      <div class="panel-body">


          <form action="" enctype="multipart/form-data" class="form-horizontal" method="post">
            <div class="preview"></div>
            <input type="file" name="image" class="form-control" style="width:30%" />
            <br/>
            <input type="text" name="kuddus">
            <button class="btn btn-success upload-image">Image Upload</button>
          </form>


      </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $(".upload-image").click(function(){
            $(".form-horizontal").ajaxForm({target: '.preview'}).submit();
        });
    }); 
</script>


</body>
</html>