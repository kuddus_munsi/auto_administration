<?php 
    include "source/header.php" ;
    include "source/sidebar.php" ;
?>


  <div class="content-wrapper">
    <section class="content-header">
      <h1>
      Attendance Sheet
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">attendance</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
          
      <div class="box">
          <div class="box-body">
            <div class="col-md-12">
            <div class="col-md-1"></div>
            <div class="col-md-3">
              <h2 class="text-center">SelectDepartment</h2>
                <div class="form-group">
                <label for="select_department_for_administrator">Department</label>
                <select  id="select_department_for_administrator" class="form-control">
                 <option value="cse">CSE</option>
                 <option value="eee">EEE</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">  
            <h2 class="text-center">Select Semester</h2>           
              <div class="form-group">
                <label for="select_semester_for_administrator">Semester</label>
                <select  id="select_semester_for_administrator" class="form-control">
                 <option value="1">1st</option>
                 <option value="2">2nd</option>
                 <option value="3">3rd</option>
                 <option value="4">4th</option>
                 <option value="5">5th</option>
                 <option value="6">6th</option>
                 <option value="7">7th</option>
                 <option value="8">8th</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <h2 class="text-center">Select Subject</h2>
              <div class="form-group">
                <label for="select_subject_for_administrator">Subject</label>
                <select  id="select_subject_for_administrator" class="form-control">
                </select>
                  
              </div>
            </div>
             <div class="col-md-2">
              <br><br><br>
              <div class="form-group">
                <label for="see_attendance_for_administrator">SEE</label>
                <input type="button" class="form-control btn btn-primary" id="see_attendance_for_administrator" value="Attendance">
              </div>
            </div>
          </div>
          </div>
          </div>
      </div>
   </section>
   <div id="todays_attendance_for_administrator"></div>

  <section class="content">
      <div class="row">
          <h1 class="text-center" style="border-right: solid 2px red;border-left: solid 2px red;border-top: solid 2px red;">See Any Attendance</h1>
      <div class="box">
          <div class="box-body">
            <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group">
                <label for="select_department_any_for_administrator">Department</label>
                <select  id="select_department_any_for_administrator" class="form-control">
                 <option value="cse">CSE</option>
                 <option value="eee">EEE</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">            
              <div class="form-group">
                <label for="select_semester_any_for_administrator">Semester</label>
                <select  id="select_semester_any_for_administrator" class="form-control">
                 <option value="1">1st</option>
                 <option value="2">2nd</option>
                 <option value="3">3rd</option>
                 <option value="4">4th</option>
                 <option value="5">5th</option>
                 <option value="6">6th</option>
                 <option value="7">7th</option>
                 <option value="8">8th</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="select_subject_any_for_administrator">Subject</label>
                <select  id="select_subject_any_for_administrator" class="form-control">
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="select_date_any_for_administrator">Date</label>
                <input type="date" class="form-control" id="select_date_any_for_administrator" value="Date">
              </div>
            </div>
             <div class="col-md-2">
              <div class="form-group">
                <label for="see_attendance_any_for_administrator">SEE</label>
                <input type="button" class="form-control btn btn-primary" id="see_attendance_any_for_administrator" value="Attendance">
              </div>
            </div>
          </div>
          </div>
          </div>
      </div>
  </section>
   <div id="any_attendance_for_administrator"></div>

  
<?php include "source/footer.php" ; ?>