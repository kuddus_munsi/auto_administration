<?php 
   require_once __DIR__.'/vendor/autoload.php';
   session_start();
   Session::checkLogin();
 ?>
 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="resource/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="resource/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="resource/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="resource/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="resource/plugins/iCheck/square/blue.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Login</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <p id="loginfail" style="color: red;display: none;">Login information is incorrect</p>

     <button id="admin_button" class="btn btn-primary">Admin</button>
     <button id="teacher_button" class="btn btn-success">Teacher</button>
     <button id="student_button" class="btn btn-secondary">Student</button>

    <div id="student">
      <div class="form-group has-feedback">
        <label for="student_roll_no">Roll</label>
        <input type="text" id="student_roll_no" class="form-control" name="student_roll_no" placeholder="Your Roll">
        <p style="display: none;color: red;" id="student_roll_alert">Roll can not be empty</p>

      </div>
      <div class="form-group has-feedback">
        <label for="department_name">Department</label>
        <select class="form-control" id="department_name" name="department_name">
          <option value="cse">CSE</option>
          <option value="eee">EEE</option>
        </select>
      </div>
      <div class="form-group has-feedback">
        <label for="student_password">Password</label>
        <input type="password" id="student_password" class="form-control" name="student_password" placeholder="Password" required>
        <p style="display: none;color: red;" id="student_password_alert">password can not be empty</p>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button id="student_login" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </div>
    <div id="teacher">
      <div class="form-group has-feedback">
        <label for="teacher_id">Teacher ID</label>
        <input type="text" id="teacher_id" class="form-control" name="teacher_id" placeholder="Teacher Id" required>
        <p id="teacher_id_alert" style="color: red;display: none;">Id can not be empty</p>
      </div>
      <div class="form-group has-feedback">
        <label for="teacher_password">Password</label>
        <input type="password" id="teacher_password" class="form-control" name="teacher_password" placeholder="Password" required>
        <p id="teacher_password_alert" style="color: red;display: none;">passord can not be empty</p>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button id="teacher_login" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
  </div>

      <div id="administrator">
        <div class="form-group has-feedback">
          <label for="administrator_id">Administrator Id</label>
          <input type="text" id="administrator_id" class="form-control" name="administrator_id" placeholder="Administrator Id" required>
          <p id="admin_id_alert" style="color: red;display: none;">admin id can not empty</p>
        </div>
        <div class="form-group has-feedback">
          <label for="administrator_password">Password</label>
          <input type="password" id="administrator_password" class="form-control" name="administrator_password" placeholder="Administrator password" required>
          <p id="admin_pass_alert" style="color: red;display: none;">admin password can not empty</p>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-xs-4">
            <button id="admin_login" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
  </div>
 

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="resource/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#student").hide();
    $("#teacher").hide();

    $("#admin_button").click(function(){
      $("#teacher").hide();
      $("#student").hide();
      $("#administrator").show();
    });
    $("#teacher_button").click(function(){
      $("#administrator").hide();
      $("#student").hide();
      $("#teacher").show();
    });
  $("#student_button").click(function(){
        $("#teacher").hide();
        $("#administrator").hide();
        $("#student").show();
      });


    
      $("#admin_login").click(function(){
        var admin_id = $("#administrator_id").val();
        var admin_password = $("#administrator_password").val();
        
          if(admin_id == "" ){
            $("#admin_id_alert").show();
            setTimeout(function(){ $('#admin_id_alert').hide();},3000);
            return false;
          }else if(admin_password == ""){
            $("#admin_pass_alert").show();
            setTimeout(function(){$('#admin_pass_alert').hide();},3000);
            return false;
          }else{
                $.ajax({
                  url:"ajax/administrator_login.php",
                  method:"POST",
                  data:{admin_id:admin_id,admin_password:admin_password},
                  success:function(data){
                    /*console.log(data);
                    return false;*/
                   if(data == "success"){
                      window.location='index.php';
                   }else if (data == "fails"){
                      $("#loginfail").show();
                      setTimeout(function(){$('#loginfail').hide();},4000);
                    }
                  }
                });
            }
    });

      $("#teacher_login").click(function(){
        var teacher_id = $("#teacher_id").val();
        var teacher_password = $("#teacher_password").val();

        if(teacher_id == ""){
          $("#teacher_id_alert").show();
          setTimeout(function(){$("#teacher_id_alert").hide();},3000);
        }else if(teacher_password == ""){
          $("#teacher_password_alert").show();
          setTimeout(function(){$("#teacher_password_alert").hide();},3000);
        }else{
          $.ajax({
            url:"ajax/teacher_login.php",
            method:"POST",
            data:{teacher_id:teacher_id,teacher_password:teacher_password},
            success:function(res){
              if(res == "success"){
                window.location='index.php';
              }else{
                $("#loginfail").show();
                setTimeout(function(){$("#loginfail").hide();},3000);
              }
            }
          });
        }
      });

      $("#student_login").click(function(){
        var student_roll_no = $("#student_roll_no").val();
        var department_name = $("#department_name").val();
        var student_password = $("#student_password").val();
        if(student_roll_no == ""){
          $("#student_roll_alert").show();
          setTimeout(function(){$("#student_roll_alert").hide();},3000);
        }else if(student_password == ""){
          $("#student_password_alert").show();
          setTimeout(function(){$("#student_password_alert").hide();},3000);
        }else{
          $.ajax({
            url:"ajax/student_login.php",
            method:"POST",
            data:{student_roll_no:student_roll_no,department_name:department_name,student_password:student_password},
            success:function(response){
              if(response == "login_success"){
                window.location = 'student-profile.php';
              }else if(response == "login_fail"){
                $("#loginfail").show();
                setTimeout(function(){$("#loginfail").hide();},3000);
              }
            }
          });
        }
      });

  });
</script>
<script>
    jQuery(
        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
<!-- Bootstrap 3.3.7 -->
<script src="resource/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="resource/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
