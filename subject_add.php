<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;


  $subject = new Subjects();
  $msg = Message::getMessage();

  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['add_subject'])){

    $storeSuject = $subject->storeSubjectInfo($_POST);
  }
 ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Subject Add</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">subject add</li>
      </ol>
    </section>
<section class="content">
  <div class="row">
  <div class="col-md-3"></div>
    <div class="col-md-6">
     <div class="box box-primary">
      <h2 class="text-center">ADD SUBJECTS</h2>

      <h2 class="text-center text-danger"><?php echo "<div id='message'> $msg</div>"?> </h2>
           <form role="form" method="post" action="" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="subject_code">Subject Code :</label>
                      <input type="text" class="form-control" id="subject_code" name="subject_code" placeholder="subject code" >
                </div>
                <div class="form-group">
                  <label for="subject_name">Subjects Name :</label>
                      <input type="text" class="form-control" id="subject_name" name="subject_name" placeholder="subject name" >
                </div>
                <div class="form-group">
                  <label for="subject_type">Subject Type</label>
                   <input type="text" class="form-control" id="subject_type" name="subject_type" placeholder="subject type" >
                </div>
                <div class="form-group">
                  <label for="subject_credit">Subject Credit</label>
                   <select  class="form-control" id="subject_credit" name="subject_credit">
                    <?php 
                      $getCredit = $subject->getSubjectCredit();
                      if($getCredit){
                        foreach ($getCredit as $value) {?>
                        <option value="<?php echo $value['subject_credit'];?>"><?php echo $value['subject_credit'];?> Credit</option>
                    <?php } } ?>
                   </select>
                </div>
                <div class="form-group">
                  <label for="subject_department">Department</label>
                  <select   class="form-control" id="subject_department" name="subject_department" >
                  <?php 
                    $getdept = $subject->getAllDepartment();
                    if($getdept){
                      foreach ($getdept as $dept) {?>
                       <option value="<?php echo $dept['department_name']; ?>"
                        >
                        <?php echo strtoupper($dept['department_name']); ?></option>
                    <?php } } ?>

                  </select>
                </div>
                <div class="form-group">
                  <label for="subject_in_semester">Semester</label>
                  <select   class="form-control" id="subject_in_semester" name="subject_in_semester" >
                  <?php 
                    $getSemester = $subject->getAllSemester();
                    if($getSemester){
                      foreach ($getSemester as $value) {?>
                       <option value="<?php echo $value['semester']; ?>"><?php echo $value['semester']; ?>th</option>
                    <?php } } ?>

                  </select>
                </div>
                <div class="form-group">
                 <button type="submit" name="add_subject" class="btn btn-primary form-control">ADD</button>
                </div>
                </div>
              </form>
</div>
<div class="col-md-3"></div>
</div>
</section>
<script>
    jQuery(
        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )

</script>
<?php include "source/footer.php" ; ?>