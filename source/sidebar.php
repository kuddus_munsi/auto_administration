 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="resource/dist/img/ustc.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Administration</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="index.php"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="index.php"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <?php 
          $admin_login = Session::get("administratorlogin");
          if($admin_login == true OR Session::get("teacherlogin") == true){
         ?>
         <?php if(Session::get("administratorlogin") == true){  ?>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>New Apply</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="new_applier_list.php"><i class="fa fa-circle-o"></i>New Applier list</a></li>
          </ul>
        </li>
      <?php  } ?>
      <?php if(Session::get("administratorlogin") == true OR Session::get("teacherlogin")== true){?>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>Students Section</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="cse_students_list.php"><i class="fa fa-circle-o"></i>CSE Student List</a></li>
            <li class="active"><a href="eee_students_list.php"><i class="fa fa-circle-o"></i>EEE Student List</a></li>
          </ul>
        </li>
      <?php  }?>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>Department,Batch</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if(Session::get("administratorlogin") == true){   ?>
            <li class="active"><a href="create-batch.php"><i class="fa fa-circle-o"></i>Create Batch</a></li>
              <?php } ?>

            <?php if(Session::get("administratorlogin") == true or Session::get("teacherlogin") == true){ ?> 
            <li class="active"><a href="batch-list.php"><i class="fa fa-circle-o"></i>Batch List</a></li>
              <?php } ?>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>Teacher Sections</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if(Session::get("administratorlogin") == true){   ?>
            <li class="active"><a href="teacher-add.php"><i class="fa fa-circle-o"></i>Add Teacher</a></li>
              <?php } ?>

            <?php if(Session::get("administratorlogin") == true or Session::get("teacherlogin") == true){ ?> 
              <li class="active"><a href="teacher-list.php"><i class="fa fa-circle-o"></i>Teacher List</a></li>
            <?php } ?>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>Subject Sections</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <?php if(Session::get("administratorlogin") == true){   ?>
                <li class="active"><a href="subject_add.php"><i class="fa fa-circle-o"></i>Add Subject</a></li>
            <?php } ?>

            <?php if(Session::get("administratorlogin") == true or Session::get("teacherlogin") == true){ ?> 
              <li class="active"><a href="subject_list_cse.php"><i class="fa fa-circle-o"></i>CSE Subject List</a></li>
              <li class="active"><a href="subject_list_eee.php"><i class="fa fa-circle-o"></i>EEE Subject List</a></li>
            <?php } ?>
          </ul>
        </li>

        <?php if(Session::get("administratorlogin") == true or Session::get("teacherlogin") == true){ ?> 
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>CSE Batch List</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <?php for ($i=25; $i <= 100; $i++) { ?>
              <li class="active"><a href="batch_wise_cse_list.php?batch_no=<?php echo $i;?>&&dept=cse"><i class="fa fa-circle-o"></i><?php echo $i;?>th Batch</a></li>
            <?php } ?>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>EEE Batch List</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <?php for ($i=15; $i <= 100; $i++) { ?>
              <li class="active"><a href="batch_wise_eee_list.php?batch_no=<?php echo $i;?>&&dept=eee"><i class="fa fa-circle-o"></i><?php echo $i;?>th Batch</a></li>
            <?php } ?>
          </ul>
        </li>
    <?php  } ?>

      <?php if(Session::get("teacherlogin") == true OR Session::get("administratorlogin") == true){ ?>
         <li class="treeview">
          <a href="#">
             <i class="fa fa-folder"></i></i> <span>Attendance Sections</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if(Session::get("teacherlogin") == true){ ?>
            <li class="active"><a href="take-cse-attendance.php"><i class="fa fa-circle-o"></i> Take CSE Attendance</a></li>
            <li class="active"><a href="take-eee-attendance.php"><i class="fa fa-circle-o"></i> Take EEE Attendance</a></li>
          <?php } ?>
          <?php if(Session::get("administratorlogin") == true){ ?>
           <li class="active"><a href="show-attendance.php"><i class="fa fa-circle-o"></i>Show Attendance</a></li>
         <?php } ?>

          </ul>
        </li>
<?php } } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>