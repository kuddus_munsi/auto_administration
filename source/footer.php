    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2019-2020 USTC.</strong> All rights
    reserved.
  </footer>




</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="resource/bower_components/jquery/dist/jquery.min.js"></script>
<!-- <script src="http://malsup.github.com/jquery.form.js"></script> --> 
<script type="text/javascript">

  $(document).ready(function(){

    /* ATTENDANCE SECTION FOR CSE STUDENT*/
    $("#semester_for_cse").blur(function(){
      var semester_no_cse = $(this).val();
      alert("ok");
      $.ajax({
          url:"ajax/ajax_request_for_cse.php",
          method:"POST",
          data:{semester_no_cse:semester_no_cse},
          success:function(response){
             $("#subject_for_cse").html(response);  
          }
      });
      });

    $("#send_attendance_request_for_cse").click(function(){
       var semester_cse = $("#semester_for_cse").val();
       var subject_code_cse = $("#subject_for_cse").val();
       var request_open_action_cse = "request_open_action_cse";
       if(typeof(subject_code_cse)==="object")
       {
        alert("Please select a subject");
       }
       else{

         $.ajax({
          url:"ajax/ajax_request_for_cse.php",
          method:"POST",
          data:{semester_cse:semester_cse,subject_code_cse:subject_code_cse,request_open_action_cse:request_open_action_cse},
          success:function(response){
             if(response == "request_ok"){
              alert("Request sent");
             }else if(response == "request already sent"){
              alert(response);
             }  
          }
      });
       }
    });

    $("#close_attendance_request_for_cse").click(function(){
       var semester_cse = $("#semester_for_cse").val();
       var subject_code_cse = $("#subject_for_cse").val();
       var request_close_action_cse = "request_close_action_cse";
       $.ajax({
          url:"ajax/ajax_request_for_cse.php",
          method:"POST",
          data:{semester_cse:semester_cse,subject_code_cse:subject_code_cse,request_close_action_cse:request_close_action_cse},
          success:function(response){
             if(response == "request_close"){
              alert(response);
             }else if(response == "request already sent"){
              alert(response);
             }  
          }
      });
    });

    /*show attendacnce for teacher for today*/
 $("#see_attendance_for_cse").click(function(){
      var semester_cse = $("#semester_for_cse").val();
      var subject_code_cse = $("#subject_for_cse").val();
      var todayatt_cse = "todayatt_cse";
      if(typeof(subject_code_cse)==="object")
       {
        alert("Please select a subject");
       }else{
          $.ajax({
            url:"ajax/ajax_request_for_cse.php",
            method:"POST",
            data:{semester_cse:semester_cse,subject_code_cse:subject_code_cse,todayatt_cse:todayatt_cse},
            success:function(response){
              $("#todays_attendance_cse").html(response);
            }
          });
       }
 });

/* //show attendance for teacher for today*/

/*load subject for any time attendance for administrator*/
     $("#select_semester_any_for_teacher").blur(function(){
      var select_semester_any_for_teacher = $(this).val();
      var select_department_any_for_teacher = $("#select_department_any_for_teacher").val();
      var any_day_att_sub = "any_day_att_sub";
      $.ajax({
          url:"ajax/ajax_request_for_cse.php",
          method:"POST",
          data:{select_semester_any_for_teacher:select_semester_any_for_teacher,select_department_any_for_teacher:select_department_any_for_teacher,
            any_day_att_sub:any_day_att_sub},
          success:function(response){
             $("#select_subject_any_for_teacher").html(response);  
          }
      });
     });
/*load subject for any time attendance for administrator*/

/*show any date attendance for teahcer*/
  $("#see_attendance_any_for_teacher").click(function(){
      var select_semester_any_for_teacher = $("#select_semester_any_for_teacher").val();
      var select_department_any_for_teacher = $("#select_department_any_for_teacher").val();
      var select_subject_any_for_teacher = $("#select_subject_any_for_teacher").val();
      var select_date_any_for_teacher = $("#select_date_any_for_teacher").val();
      select_date_any_for_teacher = select_date_any_for_teacher.replace(/^\s+/, '').replace(/\s+$/, '');
      if(typeof(select_subject_any_for_teacher) ==="object")
       {
        alert("Please select a subject");
       }else if (select_date_any_for_teacher === '') {
          alert("Please select a date");
      }else{
          $.ajax({
              url:"ajax/ajax_request_for_cse.php",
              method:"POST",
              data:{
                select_semester_any_for_teacher : select_semester_any_for_teacher,
                select_department_any_for_teacher : select_department_any_for_teacher,
                select_subject_any_for_teacher : select_subject_any_for_teacher,
                select_date_any_for_teacher : select_date_any_for_teacher},
              success:function(response){

                /*console.log(response);*/
                 $("#any_attendance_for_teacher").html(response);  
              }
          });
       }
  });
/*//show any date attendance for teahcer*/

/*monthly attendance subject load*/
    $("#select_semester_monthly_for_teacher").blur(function(){
        var select_semester_monthly_for_teacher = $(this).val();
        var select_department_monthly_for_teacher = $("#select_department_monthly_for_teacher").val();
        var monthly_att_sub = "monthly_att_sub";
        $.ajax({
            url:"ajax/ajax_request_for_cse.php",
            method:"POST",
            data:{select_semester_monthly_for_teacher:select_semester_monthly_for_teacher,select_department_monthly_for_teacher:select_department_monthly_for_teacher,
              monthly_att_sub:monthly_att_sub},
            success:function(response){
               $("#select_subject_monthly_for_teacher").html(response);  
            }
        });
     });
/*monthly attendance subject load*/

/*montly attendance laod*/
$("#see_attendance_monthly_for_teacher").click(function(){
    var select_semester_monthly_for_teacher = $("#select_semester_monthly_for_teacher").val();
      var select_department_monthly_for_teacher = $("#select_department_monthly_for_teacher").val();
      var select_subject_monthly_for_teacher = $("#select_subject_monthly_for_teacher").val();

      var select_date_from_monthly_for_teacher = $("#select_date_from_monthly_for_teacher").val();
      var select_date_to_monthly_for_teacher = $("#select_date_to_monthly_for_teacher").val();

      if(!select_subject_monthly_for_teacher)
       {
        alert("Please select a subject");
       }else if (!select_date_from_monthly_for_teacher) {
          alert("Please select  date from");
      }else if (!select_date_to_monthly_for_teacher) {
          alert("Please select date to");
      }else{
          $.ajax({
              url:"ajax/ajax_request_for_cse.php",
              method:"POST",
              data:{
                select_semester_monthly_for_teacher : select_semester_monthly_for_teacher,
                select_department_monthly_for_teacher : select_department_monthly_for_teacher,
                select_subject_monthly_for_teacher : select_subject_monthly_for_teacher,
                select_date_from_monthly_for_teacher : select_date_from_monthly_for_teacher,
                select_date_to_monthly_for_teacher : select_date_to_monthly_for_teacher,
              },
              success:function(response){

                //console.log(response);
                 $("#monthly_attendance_for_teacher").html(response);  
              }
          });
       }

});
/*montly attendance load*/

/*//ATTENDANCE SECTION FOR CSE STUDENT */


//////////////////////////////////////////////////
//////////////////////////////////////////////////
/*ATTENDANCE SECTION FOR EEE STUDENT */
    $("#semester_for_eee").blur(function(){
      var semester_no_eee = $(this).val();
      $.ajax({
          url:"ajax/show_course_by_semester_for_attendance.php",
          method:"POST",
          data:{semester_no_eee:semester_no_eee},
          success:function(response){
             $("#subject_for_eee").html(response);  
          }
      });
      });

    $("#send_attendance_request_for_eee").click(function(){
       var semester_eee = $("#semester_for_eee").val();
       var subject_code_eee = $("#subject_for_eee").val();
       var open_action_eee = "open_action_eee";
        if(!subject_code_eee)
       {
        alert("Please select a subject");
       }else{
        $.ajax({
          url:"ajax/ajax_request_for_eee.php",
          method:"POST",
          data:{semester_eee:semester_eee,subject_code_eee:subject_code_eee,open_action_eee:open_action_eee},
          success:function(response){
             if(response == "request_ok"){
              alert("Request sent");
             }else if(response == "request already sent"){
              alert(response);
             } 
          }
      });
      }

    });

    $("#close_attendance_request_for_eee").click(function(){
       var semester_eee = $("#semester_for_eee").val();
       var subject_code_eee = $("#subject_for_eee").val();
       var close_action_eee = "close_action_eee";
       $.ajax({
          url:"ajax/ajax_request_for_eee.php",
          method:"POST",
          data:{semester_eee:semester_eee,subject_code_eee:subject_code_eee,close_action_eee:close_action_eee},
          success:function(response){
             if(response == "request_close"){
              alert(response);
             }else if(response == "request already sent"){
              alert(response);
             }  
          }
      });
    });

/*show attendacnce for teacher for today*/

  $("#see_attendance_for_eee").click(function(){
      var semester_eee = $("#semester_for_eee").val();
      var subject_code_eee = $("#subject_for_eee").val();
      var todayatt_eee = "todayatt_eee";
      if(!subject_code_eee)
       {
        alert("Please select a subject");
       }else{
          $.ajax({
            url:"ajax/ajax_request_for_eee.php",
            method:"POST",
            data:{semester_eee:semester_eee,subject_code_eee:subject_code_eee,todayatt_eee:todayatt_eee},
            success:function(response){
              $("#todays_attendance_eee").html(response);
            }
          });
       }
 });
/* //show attendance for teacher for today*/

/*load subject for any time attendance for administrator*/
     $("#select_eee_semester_any_for_teacher").blur(function(){
      var select_eee_semester_any_for_teacher = $(this).val();
      var select_eee_department_any_for_teacher = $("#select_eee_department_any_for_teacher").val();
      var subject_for_anyday_eee = "subject_for_anyday_eee";
      $.ajax({
          url:"ajax/ajax_request_for_eee.php",
          method:"POST",
          data:{select_eee_semester_any_for_teacher:select_eee_semester_any_for_teacher,select_eee_department_any_for_teacher:select_eee_department_any_for_teacher,
            subject_for_anyday_eee:subject_for_anyday_eee},
          success:function(response){
             $("#select_eee_subject_any_for_teacher").html(response);  
          }
      });
     });
/*load subject for any time attendance for administrator*/

/*show any date attendance for teahcer*/
  $("#see_eee_attendance_any_for_teacher").click(function(){
      var select_eee_semester_any_for_teacher = $("#select_eee_semester_any_for_teacher").val();
      var select_eee_department_any_for_teacher = $("#select_eee_department_any_for_teacher").val();
      var select_eee_subject_any_for_teacher = $("#select_eee_subject_any_for_teacher").val();
      var select_eee_date_any_for_teacher = $("#select_eee_date_any_for_teacher").val();

      if(!select_eee_subject_any_for_teacher)
       {
        alert("Please select a subject");
       }else if (!select_eee_date_any_for_teacher) {
          alert("Please select a date");
      }else{
          $.ajax({
              url:"ajax/ajax_request_for_eee.php",
              method:"POST",
              data:{
                select_eee_semester_any_for_teacher : select_eee_semester_any_for_teacher,
                select_eee_department_any_for_teacher : select_eee_department_any_for_teacher,
                select_eee_subject_any_for_teacher : select_eee_subject_any_for_teacher,
                select_eee_date_any_for_teacher : select_eee_date_any_for_teacher},
              success:function(response){

                /*console.log(response);*/
                 $("#any_eee__attendance_for_teacher").html(response);  
              }
          });
       }
  });
/*//show any date attendance for teahcer*/

/*monthly attendance subject load*/
    $("#select_eee_semester_monthly_for_teacher").blur(function(){
        var select_eee_semester_monthly_for_teacher = $(this).val();
        var select_eee_department_monthly_for_teacher = $("#select_eee_department_monthly_for_teacher").val();
        var subject_for_month_eee = "subject_for_month_eee";
        $.ajax({
            url:"ajax/ajax_request_for_eee.php",
            method:"POST",
            data:{select_eee_semester_monthly_for_teacher:select_eee_semester_monthly_for_teacher,select_eee_department_monthly_for_teacher:select_eee_department_monthly_for_teacher,
              subject_for_month_eee:subject_for_month_eee},
            success:function(response){
               $("#select_eee_subject_monthly_for_teacher").html(response);  
            }
        });
     });
/*//monthly attendance subject load*/

/*montly attendance laod*/
$("#see_eee_attendance_monthly_for_teacher").click(function(){
    var select_eee_semester_monthly_for_teacher = $("#select_eee_semester_monthly_for_teacher").val();
      var select_eee_department_monthly_for_teacher = $("#select_eee_department_monthly_for_teacher").val();
      var select_eee_subject_monthly_for_teacher = $("#select_eee_subject_monthly_for_teacher").val();

      var select_eee_date_from_monthly_for_teacher = $("#select_eee_date_from_monthly_for_teacher").val();
      var select_eee_date_to_monthly_for_teacher = $("#select_eee_date_to_monthly_for_teacher").val();

      if(!select_eee_subject_monthly_for_teacher)
       {
        alert("Please select a subject");
       }else if (!select_eee_date_from_monthly_for_teacher) {
          alert("Please select  date from");
      }else if (!select_eee_date_to_monthly_for_teacher) {
          alert("Please select date to");
      }else{
          $.ajax({
              url:"ajax/ajax_request_for_eee.php",
              method:"POST",
              data:{
                select_eee_semester_monthly_for_teacher : select_eee_semester_monthly_for_teacher,
                select_eee_department_monthly_for_teacher : select_eee_department_monthly_for_teacher,
                select_eee_subject_monthly_for_teacher : select_eee_subject_monthly_for_teacher,
                select_eee_date_from_monthly_for_teacher : select_eee_date_from_monthly_for_teacher,
                select_eee_date_to_monthly_for_teacher : select_eee_date_to_monthly_for_teacher,
              },
              success:function(response){

                //console.log(response);
                 $("#monthly_eee_attendance_for_teacher").html(response);  
              }
          });
       }

});
/*montly attendance load*/

 /*//ATTENDANCE SECTION FOR EEE STUDENT */

///////////////////////////////////////////////////
//////////////////////////////////////////////////
/*load subject for administrator*/
    $("#select_semester_for_administrator").blur(function(){
      var select_semester_for_administrator = $(this).val();
      var select_department_for_administrator = $("#select_department_for_administrator").val();
      $.ajax({
          url:"ajax/ajax_request_for_administrator.php",
          method:"POST",
          data:{select_semester_for_administrator:select_semester_for_administrator,select_department_for_administrator:select_department_for_administrator},
          success:function(response){
                //console.log(response);

             $("#select_subject_for_administrator").html(response);  
          }
      });
      });
  /*//load subject for administrator*/

    /*load today attendance for adminnistrator */
     $("#see_attendance_for_administrator").click(function(){
      var select_semester_for_administrator = $("#select_semester_for_administrator").val();
      var select_department_for_administrator = $("#select_department_for_administrator").val();
      var select_subject_for_administrator = $("#select_subject_for_administrator").val();
      if(!select_subject_for_administrator)
       {
        alert("Please select a subject");
       }else{
          $.ajax({
              url:"ajax/ajax_request_for_administrator.php",
              method:"POST",
              data:{
                  select_semester_for_administrator:select_semester_for_administratorselect_department_for_administrator:select_department_for_administrator,select_subject_for_administrator:select_subject_for_administrator},
              success:function(response){
                console.log(response);
                 //$("#todays_attendance_for_administrator").html(response);  
              }
          });
       }
      
      });
    /*//load attendance for adminnistrator for today*/

/*load subject for any time attendance for administrator*/
     $("#select_semester_any_for_administrator").blur(function(){
      var select_semester_any_for_administrator = $(this).val();
      var select_department_any_for_administrator = $("#select_department_any_for_administrator").val();
      $.ajax({
          url:"ajax/ajax_request_for_administrator.php",
          method:"POST",
          data:{select_semester_any_for_administrator:select_semester_any_for_administrator,select_department_any_for_administrator:select_department_any_for_administrator},
          success:function(response){
             $("#select_subject_any_for_administrator").html(response);  
          }
      });
     });
/*load subject for any time attendance for administrator*/

$("#see_attendance_any_for_administrator").click(function(){
      var select_semester_any_for_administrator = $("#select_semester_any_for_administrator").val();
      var select_department_any_for_administrator = $("#select_department_any_for_administrator").val();
      var select_subject_any_for_administrator = $("#select_subject_any_for_administrator").val();
      var select_date_any_for_administrator = $("#select_date_any_for_administrator").val();

      if(!select_subject_any_for_administrator)
       {
        alert("Please select a subject");
       }else if (!select_date_any) {
          alert("Please select a date");
      }else{
          $.ajax({
              url:"ajax/ajax_request_for_administrator.php",
              method:"POST",
              data:{
                select_semester_any_for_administrator : select_semester_any_for_administrator,
                select_department_any_for_administrator : select_department_any_for_administrator,
                select_subject_any_for_administrator : select_subject_any_for_administrator,
                select_date_any_for_administrator : select_date_any_for_administrator},
              success:function(response){

                /*console.log(response);*/
                 $("#any_attendance_for_administrator").html(response);  
              }
          });
       }
});
/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
  });
</script>
<!-- show/hide attendance button -->
<script type="text/javascript">
  <?php 
    $student = new Students();
        $student_roll =Session::get("student_roll");
        $student_department = Session::get("student_dept");
        

        if($student_roll != "" && $student_department != ""){
          $getStudent_info = $student->profileByIdAndDepartment($student_roll,$student_department);
          foreach($getStudent_info as $profile){ 
            $roll = $profile['std_roll_no'];
             $dept = $profile['std_dept_name'];
          }
          $semester_no = $student->getSemesterByRoll($roll,$dept);
        } 
?>

  $(document).ready(function(){
     
     function attendancePermissionCheck(){
        var action = "attendance_chekc";
        var dept = '<?php echo $student_department?>';
        var semester = '<?php echo $semester_no?>';

          $.ajax({
          url:"ajax/attendance_status_check.php",
          method:"POST",
          data:{action:action,dept:dept,semester:semester},
          success:function (response){
            if(response == 0){
              $("#attendance_button").hide();
            }else if(response == 1){
              $("#attendance_button").show();
            }
          }
         });
     }
     setInterval(function(){attendancePermissionCheck()},300000);
    });
</script>
<!-- //show/hide attendance button -->




<!-- jQuery UI 1.11.4 -->
<script src="resource/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="resource/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="resource/bower_components/raphael/raphael.min.js"></script>
<script src="resource/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="resource/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="resource/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="resource/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="resource/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="resource/bower_components/moment/min/moment.min.js"></script>
<script src="resource/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="resource/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
      $('.select2').select2()
      //Date picker
      $('#datepicker').datepicker({
         autoclose: true
    })
     })
</script>
<!-- Bootstrap WYSIHTML5 -->
<script src="resource/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="resource/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="resource/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="resource/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="resource/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="resource/dist/js/demo.js"></script>
<script src="resource/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="resource/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
    jQuery(
        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
    $(function () {
         $('#example1').DataTable();
         $('#example2').DataTable();
     })

</script>
</body>
</html>
