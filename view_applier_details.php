<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;

    $app = new Applier();
    $msg = Message::getMessage();
    $std = new Students();
    $teacher = new Teacher();
    $batch = new Batches();

    if(isset($_GET['applier_formNo'])){

        $form_no = $_GET['applier_formNo'];
       $getSingleView = $app->showSingle($form_no);
    }

    if($_SERVER['REQUEST_METHOD'] == "POST"){
       $student_data = $std->makeStudent($_POST);
    } 

 ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <h2 class="text-center text-danger"><?php echo "<div id='message'> $msg</div>"?> </h2>
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
      
    </section>

    <section class="content">
      <div class="row">
            <div class="col-md-12">
          
                <style>
                    .font-sizeD{
                        font-size:20px;
                    }
                    .font_sizeD{
                        font-size:20px;
                    }
                </style>
                <?php 
                    if(isset($getSingleView)){
                    foreach($getSingleView as $view){  ?>
                    
                    <div class="box box-widget widget-user-2">
                    <div class="widget-user-header bg-green">
                    <div class="widget-user-image">
                        <img class="img-circle" src="<?php echo $view['p_image'];?>" alt="User Avatar">
                        <h3 class="widget-user-username"><?php echo strtoupper($view['p_name']);?></h3>
                        <br>
                    </div>
                    </div>
                    <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                            <li class="font-sizeD"><a href="#">Form ID : <span class="pull-right "><?php echo $view['form_no'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Email : <span class="pull-right "><?php echo $view['p_email'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Apply for Department : <span class="pull-right "><?php echo $view['p_adm_dept'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Fathers Name : <span class="pull-right "><?php echo $view['p_father_name'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Mothers Name : <span class="pull-right "><?php echo $view['p_mother_name'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Gardian Name : <span class="pull-right "><?php echo $view['p_guardian_name'];?></span></a></li>
                        </ul>
                            <ul class="nav nav-stacked">
                            <li class="font-sizeD" ><a href="#">Relation with Gargian : <span class="pull-right "><?php echo $view['p_guard_relation'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Prensent Address : <span class="pull-right "><?php echo $view['p_per_address'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Permanent Address : <span class="pull-right "><?php echo $view['p_pre_address'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Date Of Birth :  <span class="pull-right "><?php echo $view['p_dob'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Nationality : <span class="pull-right "><?php echo $view['p_nationality'];?></span></a></li>
                            <li class="font-sizeD" ><a href="#">Religion : <span class="pull-right "><?php echo $view['p_religion'];?></span></a></li>
                        </ul>
                        </div>
                        <hr>
                        <div class="box">
                            <div class="box-body">
                            <table class="table table-bordered">
                            <tr>
                                <th style="width: 10px">Examination</th>
                                <th style="width: 25px">Board</th>
                                <th style="width: 10px">Year</th>
                                <th style="width: 25px">School/College</th>
                                <th style="width: 10px">Divition Grade</th>
                                <th style="width: 10px">CGPA</th>
                                <th style="width: 10px">Subject</th>
                            </tr>
                                <tr class="font_sizeD">
                                <td><?php echo $view['p_ac_ssc_exam']; ?></td>
                                <td><?php echo $view['p_ac_ssc_board']; ?></td>
                                <td><?php echo $view['p_ac_ssc_year']; ?></td>
                                <td><?php echo $view['p_ac_college_ssc_school']; ?></td>
                                <td><?php echo $view['p_ac_div_ssc_grade']; ?></td>
                                <td><?php echo $view['p_ac_ssc_cgpa']; ?></td>
                                <td><?php echo $view['p_ac_ssc_subject']; ?></td>
                                
                            </tr>
                            </tr>
                                <tr class="font_sizeD">
                                <td><?php echo $view['p_ac_hsc_exam']; ?></td>
                                <td><?php echo $view['p_ac_hsc_board']; ?></td>
                                <td><?php echo $view['p_ac_hsc_year']; ?></td>
                                <td><?php echo $view['p_ac_college_hsc_school']; ?></td>
                                <td><?php echo $view['p_ac_div_hsc_grade']; ?></td>
                                <td><?php echo $view['p_ac_hsc_cgpa']; ?></td>
                                <td><?php echo $view['p_ac_hsc_subject']; ?></td>
                                
                            </tr>
                            </table>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-body">
                            <form action="" role="form" method="post" enctype="multipart/form-data">
                                <div class="col-md-6 pull-left">
                                <div class="form-group">
                                    <label for="money_reciept_code">Money Reciept</label>
                                    <input type="text" class="form-control" id="money_reciept_code" name="money_reciept_code" placeholder="enter money reciept code">
                                </div>
                                <div class="form-group">
                                    <label for="reciept_image">Money Reciept</label>
                                    <input type="file" class="form-control" id="reciept_image" name="reciept_image">
                                </div>
                                 <div class="form-group">
                                    <label for="std_supervisor">Assaign Supervisor</label>
                                    <select class="form-control" id="std_supervisor" name="std_supervisor">
                                        <?php 
                                            $getAllteacher = $teacher->getAllTeacherList();
                                            if($getAllteacher){
                                                foreach ($getAllteacher as $teachers) {
                                        ?>
                                        <option value="<?php echo $teachers['teacher_code'];?>">
                                            <?php echo strtoupper($teachers['teacher_name']);?>    
                                        </option>
                                        <?php } } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Session From:</label>
                                    <div class="input-group date">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="date" name="session_from" class="form-control" id="datepicker">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                        <label for="batch_no"> Assaign Batch </label>
                                        <select class="form-control" id="batch_no" name="batch_no">
                                            <?php 
                                                $batch_list = $batch->show();
                                                if($batch_list){
                                                    foreach ($batch_list as  $value) {?>
                                                        
                                                <option value="<?php echo $value['batch_no']; ?>">
                                                    <?php echo $value['batch_no']; ?>th</option>
                                                   <?php  }
                                                }
                                             ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 pull-right">
                                    <div class="form-group">
                                    <label for="prvious_roll">Last Inserted Roll</label>
                                        <?php 
                                            $dept_name = $view['p_adm_dept'];
                                            $getLastRoll = $std->getLastInsertedRoll($dept_name);
                                            if ($getLastRoll) {
                                                foreach ($getLastRoll as $roll) {     }
                                            }
                                         ?>
                                        <input type="text" class="form-control" id="prvious_roll" value="<?php echo $roll['std_roll_no'];?>" readonly >
                                    </div>
                                    <div class="form-group">
                                        <?php 
                                            if($dept_name == "cse"){
                                                $roll = date('ym')."01";
                                                }elseif($dept_name == "eee"){
                                                $roll = date('ym')."03";
                                            }

                                         ?>
                                        <label for="roll_no">New Roll For <?php echo $view['p_adm_dept']; ?> </label>
                                        <input type="text" class="form-control" id="roll_no" name="roll_no" value="<?php echo $roll;?>">
                                    </div>
                                     <div class="form-group">
                                        <label for="dept_floor_loc"> Department Location </label>
                                        <select class="form-control" id="dept_floor_loc" name="dept_floor_loc">
                                            <option value="1">Frist FLoor</option>
                                            <option value="2">Second FLoor</option>
                                            <option value="3">Third FLoor</option>
                                            <option value="4">Foruth FLoor</option>
                                            <option value="5">Fifth FLoor</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Session To:</label>
                                        <div class="input-group date">
                                          <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                          </div>
                                          <input type="date" name="session_to" class="form-control" id="datepicker">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php 
                                           $dept_name = $view['p_adm_dept'];
                                            $getLastRegNo = $std->getLastInsertedRregNo($dept_name);
                                            if ($getLastRegNo) {
                                                foreach ($getLastRegNo as $registration) {   
                                                    $data =  $registration['std_reg_no'];
                                                  }
                                                  $reg = explode("-",$data);      
                                                   $new_reg = $reg[1];      
                                            }
                                         ?>
                                        <label for="registration_no"> Registreation No </label>
                                          <input type="text" name="registration_no" class="form-control" id="registration_no" value="<?php echo $new_reg+1;?>">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <button type="submit"  class="btn btn-primary form-control">make student</button>
                                     </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden"   name="form_no" value="<?php echo $view['form_no'];?>">
                                </div>
                            </form>
                          </div>
                        </div>
                <?php } } ?>
            </div>
          </div>
        </div>
      </div>
   </section>
<?php include "source/footer.php" ; ?>