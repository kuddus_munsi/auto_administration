<?php 
	class Subjects extends Database
	{

		public function dateFormat($date){
        	return   date('F j, Y ,g:i a', strtotime($date));
    	}

		public  function validation($data)
    	{
			$data = trim($data);
	        $data = stripcslashes($data);
	        $data = htmlspecialchars($data);
	        return $data;
		}

		public function storeSubjectInfo($data)
		{
			$subject_name = $this->validation($data['subject_name']);
			$subject_code = $this->validation($data['subject_code']);
			$subject_type = $this->validation($data['subject_type']);
			$subject_in_semester = $this->validation($data['subject_in_semester']);
			$subject_department = $this->validation($data['subject_department']);
			$subject_credit = $this->validation($data['subject_credit']);
			$live_status = date("Y-m-d");    

			if($subject_name =="" OR $subject_code =="" OR $subject_credit == "" ){
				Message::showMessage("Error! Subject information can not empty");
			}else{
				$query = "SELECT * from subjects where subject_name = '$subjects_name' OR subject_code = '$subject_code'";
				$result = $this->select($query);
				if($result){
					Message::showMessage("Error! subject name or code already added");
				}else{
				$query = "INSERT INTO subjects (subject_in_semester,subject_name,subject_code,subject_type,subject_department,subject_credit,live_status)VALUES('$subject_in_semester','$subject_name','$subject_code','$subject_type','$subject_department','$subject_credit','$live_status')";
				$result = $this->insert($query);
				if($result){
					Message::showMessage("Success! Subject Added");
				}else{
					Message::showMessage("Error! subject not added");
				}
				}
			}
		}

		public function showFirstSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 1 AND subject_department = 'cse'";
			$result = $this->select($query);
			return $result;
		}

		public function showSecondSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 2 AND subject_department = 'cse'";
			$result = $this->select($query);
			return $result;

		}

		public function showThirdSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 3 AND subject_department = 'cse'";
			$result = $this->select($query);
			return $result;
		}

		public function showFourthSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 4 AND subject_department = 'cse'";
			$result = $this->select($query);
			return $result;

		}
		public function showFifthSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 5 AND subject_department = 'cse'";
			$result = $this->select($query);
			return $result;
		}

		public function showSixSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 6 AND subject_department = 'cse'";
			$result = $this->select($query);
			return $result;

		}

		public function showSevenSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 7 AND subject_department = 'cse'";
			$result = $this->select($query);
			return $result;

		}
		public function showEightSemesterSubjectsForCSE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 8 AND subject_department = 'cse'";
			$result = $this->select($query); 
			return $result;

		}

		///eee subject list read

		public function showFirstSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 1 AND subject_department = 'eee'";
			$result = $this->select($query);
			return $result;
		}

		public function showSecondSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 2 AND subject_department = 'eee'";
			$result = $this->select($query);
			return $result;

		}

		public function showThirdSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 3 AND subject_department = 'eee'";
			$result = $this->select($query);
			return $result;
		}

		public function showFourthSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 4 AND subject_department = 'eee'";
			$result = $this->select($query);
			return $result;

		}
		public function showFifthSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 5 AND subject_department = 'eee'";
			$result = $this->select($query);
			return $result;
		}

		public function showSixSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 6 AND subject_department = 'eee'";
			$result = $this->select($query);
			return $result;

		}

		public function showSevenSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 7 AND subject_department = 'eee'";
			$result = $this->select($query);
			return $result;

		}
		public function showEightSemesterSubjectsForEEE()
		{
			$query = "SELECT * FROM subjects where subject_in_semester = 8 AND subject_department = 'eee'";
			$result = $this->select($query); 
			return $result;

		}

		///eee subject lsit read

		public function getAllSemester()
		{
			$query = "SELECT * FROM semesters";
			$result = $this->select($query);
			return $result;
		}

		public function deleteSubject($id)
		{
			$query = "DELETE FROM  subjects WHERE id = '$id'";
			$result = $this->delete($query);
			if($result){
				Message::showMessage("Success! subject deleted successfully");
			}
		}

		public function getSubjectByCode($subject_id)
		{
			$query = "SELECT * FROM subjects WHERE id = '$subject_id'";
			$result = $this->select($query);
			return $result;
		}

		public function getAllDepartment()
		{
			$query = "SELECT * FROM departments";
			return $this->select($query);
		}

		public function updateSubjectInfo_cse($data)
		{

			$subject_id = $this->validation($data['id']);
			$subject_name = $this->validation($data['subject_name']);
			$subject_code = $this->validation($data['subject_code']);
			$subject_type = $this->validation($data['subject_type']);
			$subject_in_semester = $this->validation($data['subject_in_semester']);
			$subject_department = $this->validation($data['subject_department']);
			$subject_credit = $this->validation($data['subject_credit']);

			if($subject_name =="" OR $subject_code =="" OR $subject_credit == "" ){
				Message::showMessage("Error! Subject information can not empty");
			}else{

				$query = "SELECT * FROM subjects WHERE id = '$subject_id'";
				$result = $this->select($query);
				if($result){
					foreach ($result as  $value) {
						$subject_name_old = $value['subject_name'];
						$subject_code_old = $value['subject_code'];
						$subject_type_old = $value['subject_type'];
						$subject_in_semester_old = $value['subject_in_semester'];
						$subject_department_old = $value['subject_department'];
						$subject_credit_old = $value['subject_credit'];
						$subject_live_status_old = $value['live_status'];
					}
					$live_status_to = date("Y-m-d");
					$query = "INSERT INTO old_syllabus_subjects (subject_in_semester,subject_name,subject_code,subject_type,subject_department,subject_credit,live_from,live_to)VALUES('$subject_in_semester_old','$subject_name_old','$subject_code_old','$subject_type_old','$subject_department_old','$subject_credit_old','$subject_live_status_old','$live_status_to')";
					$insert_old_sub = $this->insert($query);
					if($insert_old_sub){
						$query = "UPDATE subjects SET 
						subject_in_semester = '$subject_in_semester',
						subject_name = '$subject_name',
						subject_code = '$subject_code',
						subject_type = '$subject_type',
						subject_department = '$subject_department',
						subject_credit  = '$subject_credit',
						live_status = '$live_status_to'
						WHERE id = '$subject_id'";
						$result = $this->update($query);
						if($result){
								echo "<script>window.location = 'subject_list_cse.php'; </script>";
							
						}else{
							Message::showMessage("Error! Subject Not Updated");
						}
					}

				}
			}
		}

		public function showCourseBySemester($semester)
		{
			
				$course_details = "";
				$query = "SELECT * from subjects WHERE subject_in_semester= '$semester'";
				$show_subject = $this->select($query);

				$course_details.='<section class="content">
						          <div class="row">          
						          <div class="box">
						                <div class="box-body">
						                  <h2 class="text-danger">Your Current Course</h2>
						                  <table id="example" class="table table-bordered table-striped">
						                    <thead>
						                    <tr>
						                      <th>Serial</th>
						                      <th>Subject Name</th>
						                      <th>Subject Code</th>
						                      <th>Subject Type</th>
						                    </tr>
						                    </thead>
						                    <tbody>';

	            foreach($show_subject as $key=>$subjects_1ist){$key+=1; 
		            $course_details.= "<tr>
			                        <td>".$key."</td>
			                        <td>".$subjects_1ist['subject_name']."</td>
			                        <td>".$subjects_1ist['subject_code']."</td>
			                        <td>".$subjects_1ist['subject_type']."</td>
			                    </tr>";
			                }
			                $course_details.="</tbody>
			                <tfoot>
			                <tr>
			                  <th>Semester</th>
			                  <th>Subject Name</th>
			                  <th>Subject Code</th>
			                  <th>Subject Type</th>
			                </tr>
			                </tfoot>
			              </table>
			            </div>
			          </div>
			          </div>
			        </section>";

				echo $course_details;
			
		}


		public function getSubjectCredit()
		{
			$query = "SELECT * FROM subject_credit ORDER BY id  ASC";
			return $result = $this->select($query);
		}

////////////////////////////////////////////////	
	}