<?php 
	class Students extends Database
	{
		

		public function dateFormat($date){
        	return   date('F j, Y', strtotime($date));
    	}

		public  function validation($data)
    	{
			$data = trim($data);
	        $data = stripcslashes($data);
	        $data = htmlspecialchars($data);
	        return $data;
		}

		public function getLastInsertedRoll($dept_name)
		{
			$query = "SELECT std_roll_no from students where std_dept_name = '$dept_name' order by std_roll_no desc limit 1";
			$roll = $this->select($query);
			return $roll;
		}

		public function makeStudent($data)
		{
			
		
			$roll_no = $this->validation($data['roll_no']);
			$roll_no = (int)$roll_no;
			$money_reciept_code =  $this->validation($data['money_reciept_code']);
			$form_no = $this->validation($data['form_no']);
			$supervisor = $this->validation($data['std_supervisor']);
			$dept_floor_loc = $this->validation($data['dept_floor_loc']);
			$batch_no = $this->validation($data['batch_no']);
			$session_from = $this->validation($data['session_from']);
			$session_to = $this->validation($data['session_to']);
			$registration_no = $this->validation($data['registration_no']);
			

			$permitted = array('jpg' , 'jpeg' , 'png');
			$file_Name = $_FILES['reciept_image']['name'];
			$file_Size = $_FILES['reciept_image']['size'];
			$file_Temp = $_FILES['reciept_image']['tmp_name'];
			$div = explode('.',$file_Name);
			$file_ext = strtolower(end($div));
			$unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
			$payorder_img = "reciept_image/".$unique_image;
			move_uploaded_file($file_Temp ,$payorder_img);

			
			
		
			if($money_reciept_code == "" OR $file_Name == ""){
				Message::showMessage("reciept code and image must can not be empty");
			} else{
				$sqlSelectQuery = "SELECT persons.*, persons_details.*, persons_academic_qualification.* from persons left join persons_details on persons.form_no = persons_details.form_no left join persons_academic_qualification on persons_academic_qualification.form_no = persons.form_no where persons.form_no = '$form_no'";
				$student_SingleInfo = $this->select($sqlSelectQuery);
				if($student_SingleInfo){
					foreach($student_SingleInfo as $info){
						

						$form_no =$info['form_no'];
						$student_name = $info['p_name'];
						$student_email = $info['p_email'];
						$department =$info['p_adm_dept'];
						$student_phone = $info['p_phone'];
						$student_image = $info['p_image'];
						//details
						$father_name = $info['p_father_name'];
						$mother_name = $info['p_mother_name'];
						$gardians_name = $info['p_guardian_name'];
						$rel_with_gardian = $info['p_guard_relation'];
						$present_address = $info['p_pre_address'];
						$permanent_address = $info['p_per_address'];
						$date_of_birth = $info['p_dob'];
						$nationality = $info['p_nationality'];
						$religion = $info['p_religion'];
						$p_study_gap = $info['p_study_gap'];
						//ssc result
						$ssc_examination = $info['p_ac_ssc_exam'];
						$ssc_board = $info['p_ac_ssc_board'];
						$ssc_year = $info['p_ac_ssc_year'];
						$ssc_school_college = $info['p_ac_college_ssc_school'];
						$ssc_divition_grade = $info['p_ac_div_ssc_grade'];
						$ssc_cgpa =$info['p_ac_ssc_cgpa'];
						$ssc_subject = $info['p_ac_ssc_subject'];
						// hsc result
						$hsc_examination = $info['p_ac_hsc_exam'];
						$hsc_board = $info['p_ac_hsc_board'];
						$hsc_year = $info['p_ac_hsc_year'];
						$hsc_school_college = $info['p_ac_college_hsc_school'];
						$hsc_divition_grade =$info['p_ac_div_hsc_grade'];
						$hsc_cgpa = $info['p_ac_hsc_cgpa'];
						$hsc_subject = $info['p_ac_hsc_subject'];

						////automatic roll

						
						
						///automatic roll


			

				if($department == "cse"){
					$registration_no_final = "cse-".$registration_no;
				$sqlInsertQuery = "INSERT INTO students(std_roll_no,std_form_no,std_name,std_reg_no,std_email,std_dept_name,std_phone,std_image,std_pay_order,std_payorder_img) VALUES ('$roll_no','$form_no','$student_name','$registration_no_final','$student_email','$department','$student_phone','$student_image','$money_reciept_code','$payorder_img')";
		        $result= $this->insert($sqlInsertQuery);

		       if($result){
            		$sqlQuery = "INSERT INTO students_details (std_roll_no,std_father_name,std_mother_name,std_guardian_name,std_guard_relation,std_per_address,std_pre_address,std_nationality,std_religion,std_dob,std_study_gap) VALUES ('$roll_no','$father_name','$mother_name','$gardians_name','$rel_with_gardian','$permanent_address','$present_address','$nationality','$religion','$date_of_birth','$p_study_gap')";
		        $success= $this->insert($sqlQuery);
		        	if($success){
		        		$sqlQuery = "INSERT INTO students_academic_qualifications (
		        		std_roll_no,
		        		std_ac_ssc_exam,
		        		std_ac_ssc_board,
		        		std_ac_ssc_year,
		        		std_ac_college_ssc_school,
		        		std_ac_div_ssc_grade,
		        		std_ac_ssc_cgpa,
		        		std_ac_ssc_subject,
		        		std_ac_hsc_exam,
		        		std_ac_hsc_board,
		        		std_ac_hsc_year,
		        		std_ac_college_hsc_school,
		        		std_ac_div_hsc_grade,
		        		std_ac_hsc_cgpa,
		        		std_ac_hsc_subject) VALUES (
		        		'$roll_no','$ssc_examination','$ssc_board','$ssc_year','$ssc_school_college','$ssc_divition_grade','$ssc_cgpa','$ssc_subject','$hsc_examination','$hsc_board','$hsc_year','$hsc_school_college','$hsc_divition_grade','$hsc_cgpa','$hsc_subject')";
		        		$done = $this->insert($sqlQuery);
		        		if($done){
		        			$query = "INSERT INTO student_department_details(std_roll_no,std_supervisor,std_dept_location,std_batch_no,std_session_from,std_session_to,std_semester_no)VALUES('$roll_no','$supervisor','$dept_floor_loc','$batch_no','$session_from','$session_to',1)";
		        			$this->insert($query);
		        			Message::showMessage("Success!! All data inserted successfully");
		        		}else{
		        			Message::showMessage("Error! Data has been not inserted");
		        		}
		        	}else{
		        		Message::showMessage("Error! Qualification data has not inserted ");
		        	}
        		}else{
            		Message::showMessage("Error! Details data has been not inserted");
        		}
			}elseif($department == "eee"){
				$registration_no_final = "eee-".$registration_no;
				$sqlInsertQuery = "INSERT INTO eee_students(std_roll_no,std_form_no,std_name,std_reg_no,std_email,std_dept_name,std_phone,std_image,std_pay_order,std_payorder_img) VALUES ('$roll_no','$form_no','$student_name','$registration_no_final','$student_email','$department','$student_phone','$student_image','$money_reciept_code','$payorder_img')";
		        $result= $this->insert($sqlInsertQuery);

		       if($result){
            		$sqlQuery = "INSERT INTO eee_students_details (std_roll_no,std_father_name,std_mother_name,std_guardian_name,std_guard_relation,std_per_address,std_pre_address,std_nationality,std_religion,std_dob,std_study_gap) VALUES ('$roll_no','$father_name','$mother_name','$gardians_name','$rel_with_gardian','$permanent_address','$present_address','$nationality','$religion','$date_of_birth','$p_study_gap')";
		        $success= $this->insert($sqlQuery);
		        	if($success){
		        		$sqlQuery = "INSERT INTO eee_students_academic_qualifications (
		        		std_roll_no,
		        		std_ac_ssc_exam,
		        		std_ac_ssc_board,
		        		std_ac_ssc_year,
		        		std_ac_college_ssc_school,
		        		std_ac_div_ssc_grade,
		        		std_ac_ssc_cgpa,
		        		std_ac_ssc_subject,
		        		std_ac_hsc_exam,
		        		std_ac_hsc_board,
		        		std_ac_hsc_year,
		        		std_ac_college_hsc_school,
		        		std_ac_div_hsc_grade,
		        		std_ac_hsc_cgpa,
		        		std_ac_hsc_subject) VALUES (
		        		'$roll_no','$ssc_examination','$ssc_board','$ssc_year','$ssc_school_college','$ssc_divition_grade','$ssc_cgpa','$ssc_subject','$hsc_examination','$hsc_board','$hsc_year','$hsc_school_college','$hsc_divition_grade','$hsc_cgpa','$hsc_subject')";
		        		$done = $this->insert($sqlQuery);
		        		if($done){
		        			$query = "INSERT INTO eee_student_department_details(std_roll_no,std_supervisor,std_dept_location,std_batch_no,std_session_from,std_session_to)VALUES('$roll_no','$supervisor','$dept_floor_loc','$batch_no','$session_from','$session_to')";
		        			$this->insert($query);
		        			Message::showMessage("Success!! All data inserted successfully");
		        		}else{
		        			Message::showMessage("Error! Data has been not inserted");
		        		}
		        	}else{
		        		Message::showMessage("Error! Qualification data has not inserted ");
		        	}
        		}else{
            		Message::showMessage("Error! Details data has been not inserted");
        		}
			}

			}//foreach end


		   }
		}

		}

		public function showCSEStudents()
		{
			$sqlQuery = "SELECT students.*,  student_department_details.* from students  left join student_department_details on student_department_details.std_roll_no = students.std_roll_no";
			$studentList = $this->select($sqlQuery);
			return $studentList;
		}
		public function showEEEStudents()
		{
			$sqlQuery = "SELECT eee_students.*,  eee_student_department_details.* from eee_students  left join eee_student_department_details on eee_student_department_details.std_roll_no = eee_students.std_roll_no";
			$studentList = $this->select($sqlQuery);
			return $studentList;
		}

		public function showSingleInfoOfCSEstudent($student_roll)
		{
			$sqlSelectQuery = "SELECT students.*, students_details.*,student_department_details.*, students_academic_qualifications.* from students left join students_details on students.std_roll_no = students_details.std_roll_no left join students_academic_qualifications on students_academic_qualifications.std_roll_no = students.std_roll_no left join student_department_details on student_department_details.std_roll_no = students.std_roll_no where students.std_roll_no = '$student_roll'";
				return $student_SingleInfo = $this->select($sqlSelectQuery);


				
		}

		public function showSingleInfoOfEEEstudent($student_roll)
		{
			$sqlSelectQuery = "SELECT eee_students.*, eee_students_details.*,eee_student_department_details.*, eee_students_academic_qualifications.* from eee_students left join eee_students_details on eee_students.std_roll_no = eee_students_details.std_roll_no left join eee_students_academic_qualifications on eee_students_academic_qualifications.std_roll_no = eee_students.std_roll_no left join eee_student_department_details on eee_student_department_details.std_roll_no = eee_students.std_roll_no where eee_students.std_roll_no = '$student_roll'";
				return $student_SingleInfo = $this->select($sqlSelectQuery);


				
		}



		public function showStudentDeptAndBatchWise($batch,$dept)
		{
			if($dept == "cse"){
				$sqlQuery = "SELECT students.*,  student_department_details.* from student_department_details  left join students on students.std_roll_no = student_department_details.std_roll_no where student_department_details.std_batch_no='$batch'";
				$studentList = $this->select($sqlQuery);
				return $studentList;
			}elseif ($dept == "eee"){
				$sqlQuery = "SELECT eee_students.*,  eee_student_department_details.* from eee_student_department_details  left join eee_students on eee_students.std_roll_no = eee_student_department_details.std_roll_no where  eee_student_department_details.std_batch_no='$batch'";
				$studentList = $this->select($sqlQuery);
				return $studentList;
			}
		}

		public function getLastInsertedRregNo($dept_name)
		{
			$query = "SELECT std_reg_no from students where std_dept_name = '$dept_name' order by std_reg_no desc limit 1";
			$reg_no = $this->select($query);
			return $reg_no;
		}


		public function studentLogin($student_roll_no,$department_name,$student_password)
		{


			$student_roll_no = $this->validation($student_roll_no);
			$student_department = $this->validation($department_name);
			$student_password = $this->validation($student_password);

			if($student_roll_no == "" OR $student_department == "" OR $student_password ==""){
				Message::showMessage("Error! Input mustbe provided to login");
			}else{
				$password = md5($student_password);
				if($student_department == "cse"){
					$query = "SELECT * FROM students WHERE std_roll_no = '$student_roll_no' AND password = '$password'";
					$result = $this->select($query);
					if($result){
						foreach ($result as  $value) {

							Session::set("studentlogin",true);
							Session::set("student_roll",$value['std_roll_no']);
							Session::set("student_dept",$value['std_dept_name']);
							Session::set("student_name",$value['std_name']);

							}
							echo "login_success";
					}
					else{
						echo "login_fail";
					}
				}elseif($student_department == "eee"){

					$query = "SELECT * FROM eee_students WHERE std_roll_no = '$student_roll_no' AND password = '$password'";
					$result = $this->select($query);
					if($result){
						foreach ($result as  $value) {

							Session::set("studentlogin",true);
							Session::set("student_roll",$value['std_roll_no']);
							Session::set("student_dept",$value['std_dept_name']);
						}
						echo "login_success";
					}

					else{
						echo "login_fail";
					}
				}
			}
		}

		public function studentProfile($student_roll,$student_department)
		{
			if($student_department == "cse")
			{
				$query = "SELECT * FROM students WHERE std_roll_no = '$student_roll'";
				return $this->select($query);


			}elseif($student_department == "eee")
			{
				$query = "SELECT * FROM eee_students WHERE std_roll_no = '$student_roll'";
				return $this->select($query);

			}
		}

		public function getSemesterByRoll($roll,$dept)
		{
			if($dept == "cse"){
				$query = "SELECT std_semester_no FROM student_department_details WHERE std_roll_no = '$roll'";
				$semester_data =  $this->select($query);
				foreach ($semester_data as  $value) {
					$semester = $value['std_semester_no'];
				}
				return $semester;
			}elseif($dept == "eee"){
				$query = "SELECT std_semester_no FROM eee_student_department_details WHERE std_roll_no = '$roll'";
				$semester_data =  $this->select($query);
				foreach ($semester_data as  $value) {
					$semester = $value['std_semester_no'];
				}
				return $semester;
			}
		}

		public function checkAttendanceStatus($semester_no,$dept)
		{
			if($dept == "cse"){
				$query = "SELECT status FROM cse_simple_status_table WHERE semester = '$semester_no'";
				$status_data =  $this->select($query);
				foreach ($status_data as  $value) {
					$status = $value['status'];
				}
				return $status;
			}elseif($dept == "eee"){
				$query = "SELECT status FROM eee_simple_status_table WHERE semester = '$semester_no'";
				$status_data =  $this->select($query);
				foreach ($status_data as  $value) {
					$status = $value['status'];
				}
				return $status;
			}
		}

		public function submitAttendanceToday($roll,$dept,$date,$batch,$semester,$subject_code)
		{
			if($dept == "cse")
			{	
				$query = "SELECT * FROM cse_student_attendance WHERE std_roll = '$roll' AND attendance_date='$date' AND subject_code = '$subject_code'";
				$resumit = $this->select($query);
				if($resumit){
					echo "resubmit";
				}else{
					$query = "INSERT INTO cse_student_attendance (std_roll,std_batch_no,std_semester_no,std_attendance,attendance_date,subject_code)VALUES('$roll','$batch','$semester','yes','$date','$subject_code')";
					$result = $this->insert($query);
					if($result){
						echo "success";
					}
				}
			}elseif($dept == "eee"){
				$query = "SELECT * FROM eee_student_attendance WHERE std_roll = '$roll' AND attendance_date='$date' AND subject_code='$subject_code'";
				$resumit = $this->select($query);
				if($resumit){
					echo "resubmit";
				}else{
					$query = "INSERT INTO eee_student_attendance (std_roll,std_batch_no,std_semester_no,std_attendance,attendance_date,subject_code)VALUES('$roll','$batch','$semester','yes','$date','$subject_code')";
					$result = $this->insert($query);
					if($result){
						echo "success";
					}
				}
			}
		}

		public function profileByIdAndDepartment($roll,$dept)
		{
			if($dept == "cse")
			{
				$sqlSelectQuery = "SELECT students.*, students_details.*,student_department_details.*, students_academic_qualifications.* from students left join students_details on students.std_roll_no = students_details.std_roll_no left join students_academic_qualifications on students_academic_qualifications.std_roll_no = students.std_roll_no left join student_department_details on student_department_details.std_roll_no = students.std_roll_no where students.std_roll_no = '$roll'";
					return $student_SingleInfo = $this->select($sqlSelectQuery);

			}elseif ($dept == "eee") {
				$sqlSelectQuery = "SELECT eee_students.*, eee_students_details.*,eee_student_department_details.*, eee_students_academic_qualifications.* from eee_students left join eee_students_details on eee_students.std_roll_no = eee_students_details.std_roll_no left join eee_students_academic_qualifications on eee_students_academic_qualifications.std_roll_no = eee_students.std_roll_no left join eee_student_department_details on eee_student_department_details.std_roll_no = eee_students.std_roll_no where eee_students.std_roll_no = '$roll'";
					return $student_SingleInfo = $this->select($sqlSelectQuery);
			}
 		}
    }