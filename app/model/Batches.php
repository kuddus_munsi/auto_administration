<?php 
	class Batches extends Database
	{
		public $student_id;

		public function dateFormat($date){
        	return   date('F j, Y ,g:i a', strtotime($date));
    	}

		public  function validation($data)
    	{
			$data = trim($data);
	        $data = stripcslashes($data);
	        $data = htmlspecialchars($data);
	        return $data;
        }

        public function storBatch($data)
        {
            $batch_no = $this->validation($data['batch_no']);

            $sqlQuery = "INSERT INTO batches(batch_no) VALUES ('$batch_no')";
            $result = $this->insert($sqlQuery);
            if($result){
                Message::showMessage("success!! batch created");
            }
     
        }


        public function show()
        {
            $sqlQuery = "SELECT * FROM batches order by id asc ";
			$batches = $this->select($sqlQuery);
			return $batches;
        }

        public function delete($id)
        {
            $sqlQuery = "DELETE from batches WHERE id = '$id'";
            $result= $this->delete($sqlQuery);
            if($result){
                echo "<script>window.location = 'batch-list.php'</script>";
            }
    
        }
    
    }