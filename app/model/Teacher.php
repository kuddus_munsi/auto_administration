<?php 

class Teacher extends Database
{
	public  function validation($data)
	{
		$data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
	}

	public function storeInfo($data)
	{

		$teacher_name = $this->validation($data['teacher_name']);
		$teacher_code = $this->validation($data['teacher_code']);
		$teacher_designation = $this->validation($data['teacher_designation']);
		$teacher_department = $this->validation($data['teacher_department']);

		$permitted = array('jpg' , 'jpeg' , 'png');
		$file_Name = $_FILES['teacher_image']['name'];
		$file_Size = $_FILES['teacher_image']['size'];
		$file_Temp = $_FILES['teacher_image']['tmp_name'];
		$div = explode('.',$file_Name);
		$file_ext = strtolower(end($div));
		$unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
		$teacher_image = "tImage/".$unique_image;
		move_uploaded_file($file_Temp ,$teacher_image);

		if($teacher_name =="" OR $teacher_code =="" OR $teacher_department=="" OR $teacher_designation=="" OR $teacher_image==""){
			Message::showMessage("Error!! Input can not be empty");
		}elseif($file_Size > 3000000){
				Message::showMessage("Image size shoul be less than 300kb");
		}
		elseif (in_array($file_ext , $permitted) === false){
				Message::showMessage("Error!! you can upload only :-".implode(',' , $permitted));
		}else{

			$query = "select * from teachers where teacher_code = '$teacher_code'";
			$result = $this->select($query);
			if($result){
				Message::showMessage("Error! Teacher Code already assaigned");
			}else{
			$query = "INSERT INTO teachers (teacher_name,teacher_code,teacher_designation,teacher_department,teacher_image)VALUES('$teacher_name','$teacher_code','$teacher_designation','$teacher_department','$teacher_image')";
			$result = $this->insert($query);
			if($result){
				Message::showMessage("Success!! Teacher Information Added");
			}else{
				Message::showMessage("Error!! someting went wrong, please try again");
			}
		}

		}


	}

	public function getAllTeacherList()
	{
		$query = "SELECT * FROM teachers";
		$result = $this->select($query);
		return $result;
	}

	public function deleteTeacher($teacher_code)
	{
		$query = "SELECT teacher_image FROM teachers where teacher_code = '$teacher_code'";
		$result = $this->select($query);
		foreach ($result as  $value) {
			$image = $value['teacher_image'];
		}
		if(unlink($image)){

			$query = "DELETE FROM teachers where teacher_code = '$teacher_code'";
			$result = $this->delete($query);
			if($result){
				Message::showMessage("Success!! Teacher Information Deleted");
			}else{
				var_dump("no");
			}
		}
	}

	public function getSingleTeacerInfo($teacher_code)
	{

		$query = "SELECT * FROM teachers where teacher_code = '$teacher_code'";
		$result = $this->select($query);
		return $result;
	}

	public function updateTeacherInfo($data)
	{

		$teacher_name = $this->validation($data['teacher_name']);
		$teacher_code = $this->validation($data['teacher_code']);
		$teacher_designation = $this->validation($data['teacher_designation']);
		$teacher_department = $this->validation($data['teacher_department']);
		$teacher_previous_code = $this->validation($data['teacher_previous_code']);

		$permitted = array('jpg' , 'jpeg' , 'png');
		$file_Name = $_FILES['teacher_image']['name'];
		$file_Size = $_FILES['teacher_image']['size'];
		$file_Temp = $_FILES['teacher_image']['tmp_name'];
		$div = explode('.',$file_Name);
		$file_ext = strtolower(end($div));
		$unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
		$teacher_image = "tImage/".$unique_image;
		move_uploaded_file($file_Temp ,$teacher_image);

		if($teacher_name =="" OR $teacher_code =="" OR $teacher_department=="" OR $teacher_designation==""){
			Message::showMessage("Error!! Input can not be empty");
		}elseif($file_Size > 3000000){
				Message::showMessage("Image size shoul be less than 300kb");
		}else{
			if($file_Name != ""){
				$query = "UPDATE  teachers SET 
				teacher_name = '$teacher_name',
				teacher_code = '$teacher_code',
				teacher_designation = '$teacher_designation',
				teacher_department = '$teacher_department',
				teacher_image = '$teacher_image'
				where teacher_code = '$teacher_previous_code'";
			$result = $this->update($query);
			if($result){
				Message::showMessage("Success! Teacher Profile Updated");
			}else{
				Message::showMessage("Error1");
			}
			}else{
				$query = "UPDATE  teachers SET 
				teacher_name = '$teacher_name',
				teacher_code = '$teacher_code',
				teacher_designation = '$teacher_designation',
				teacher_department = '$teacher_department'
				where teacher_code = '$teacher_previous_code'";
			$result = $this->update($query);
			if($result){
				Message::showMessage("Success! Teacher Profile Updated");
			}else{
				Message::showMessage("Error!");
			}

			}
		}
	}

	public function teacherLogin($teacher_id,$teacher_password)
	{
		$teacher_id = $this->validation($teacher_id);
		$teacher_password = md5($this->validation($teacher_password));

		$query = "SELECT * FROM teachers WHERE teacher_code='$teacher_id' AND teachers_password='$teacher_password'";
		$result = $this->select($query);
		if($result){
			foreach ($result as $key => $value) {
			}
			Session::set("teacherlogin",true);
			Session::set("teacher_id",$value['teacher_code']);
			Session::set("panel","Teacher");
			Session::set("teacher_name",$value['teacher_name']);
			Session::set("teacher_role",$value['teacher_role']);
			echo "success";
		}else{
			echo "fail";
		}
	}

	public function getTeacherName($teacher_code){
		$query = "SELECT * FROM teachers WHERE teacher_code = '$teacher_code'";
		$result = $this->select($query);
		foreach ($result as  $value) {
			$name = $value['teacher_name'];
		}
		return $name;
	}

}