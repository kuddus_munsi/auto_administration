<?php 

class Attendance extends Database
{
	public function dateFormat($date){
        return   date('F j, Y ,g:i a', strtotime($date));
    }

	public  function validation($data)
	{
		$data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
	}

	public function showCourseBySemesterForCSEAttendance($semester_no)
		{

			if($semester_no != ""){
				$course = "";
				$query = "SELECT * from subjects WHERE subject_in_semester= '$semester_no' AND subject_department = 'cse'";
				$result = $this->select($query);
				if($result){
					foreach ($result as  $value) {
						$course.= '<option value="'.$value['subject_code'].'">'.$value['subject_name'].'</option>';
					
					}
				}
				echo $course;
			}
		}
		public function showCourseBySemesterForEEEAttendance($semester_no)
		{

			if($semester_no != ""){
				$course = "";
				$query = "SELECT * from subjects WHERE subject_in_semester= '$semester_no' AND subject_department = 'eee'";
				$result = $this->select($query);
				if($result){
					foreach ($result as  $value) {
						$course.= '<option value="'.$value['subject_code'].'">'.$value['subject_name'].'</option>';
					
					}
				}
				echo $course;
			}
		}


		public function sendAttendanceRequestForCSE($semester,$subject_code)
		{

			$semester = $this->validation($semester);
			$subject_code = $this->validation($subject_code);
			$query = "SELECT * FROM cse_temp_attendance_subject_code WHERE semester = '$semester' AND subject_code='$subject_code'";
			$result = $this->select($query);
			if($result){
				echo "request already sent";
			}
			else{
				$query = "INSERT INTO cse_temp_attendance_subject_code(semester,subject_code)VALUES('$semester','$subject_code')";
				$result = $this->insert($query);
				if($result){
					$query = "UPDATE cse_simple_status_table SET status = 1 WHERE semester = '$semester'";
					$update_status = $this->update($query);
					if($update_status){
						echo "request_ok";
					}
				}
			}
		}


		public function closeAttendanceRequestForCSE($semester,$subject_code){
			$semester = $this->validation($semester);
			$subject_code = $this->validation($subject_code);
			$query = "DELETE FROM cse_temp_attendance_subject_code WHERE semester = '$semester' AND subject_code ='$subject_code'";
			$result = $this->delete($query);
			if($result){
				$query = "UPDATE cse_simple_status_table SET status = 0 WHERE semester = '$semester'";
				$update_status = $this->update($query);
				if($update_status){
					echo "request_close";
				}
			}
		}

		public function sendAttendanceRequestForEEE($semester,$subject_code)
		{

			$semester = $this->validation($semester);
			$subject_code = $this->validation($subject_code);
			$query = "SELECT * FROM eee_temp_attendance_subject_code WHERE semester = '$semester' AND subject_code='$subject_code'";
			$result = $this->select($query);
			if($result){
				echo "request already sent";
			}else{
				$query = "INSERT INTO eee_temp_attendance_subject_code(semester,subject_code)VALUES('$semester','$subject_code')";
				$result = $this->insert($query);
				if($result){
					$query = "UPDATE eee_simple_status_table SET status = 1 WHERE semester = '$semester'";
					$update_status = $this->update($query);
					if($update_status){
						echo "request_ok";
					}
				}
			}
		}


		public function closeAttendanceRequestForEEE($semester,$subject_code){
			$semester = $this->validation($semester);
			$subject_code = $this->validation($subject_code);
			$query = "DELETE FROM eee_temp_attendance_subject_code WHERE semester = '$semester' AND subject_code ='$subject_code'";
			$result = $this->delete($query);
			if($result){
				$query = "UPDATE eee_simple_status_table SET status = 0 WHERE semester = '$semester'";
				$update_status = $this->update($query);
				if($update_status){
					echo "request_close";
				}
			}
		}

		public function getCourseIdForAttendance($semester_no,$dept)
		{
			if($dept == "cse"){
				$query = "SELECT subject_code FROM  cse_temp_attendance_subject_code WHERE semester = '$semester_no'";
				$result = $this->select($query);
				if($result){
					foreach ($result as  $value) {
						$course_code = $value['subject_code'];
					}
					return $course_code;
				}
			}elseif($dept == "eee"){
			$query = "SELECT subject_code FROM  eee_temp_attendance_subject_code WHERE semester = '$semester_no'";
			$result = $this->select($query);
			if($result){
				foreach ($result as  $value) {
					$course_code = $value['subject_code'];
				}
				return $course_code;
			}
			}
		}

		public function chekcAttendancePermissionStatus($dept,$semester){
			if($dept == "cse"){
				$query = "SELECT status FROM cse_simple_status_table WHERE semester = '$semester'";
				$select = $this->select($query);
				if($select){
					foreach ($select as  $value) {
						$status = $value['status'];
					}
					echo $status;
				}
			}elseif($dept == "eee"){
				$query = "SELECT status FROM eee_simple_status_table WHERE semester = '$semester'";
				$select = $this->select($query);
				if($select){
					foreach ($select as  $value) {
						$status = $value['status'];
					}
					echo $status;
				}
			}
		}

		public function getTodaysAttendanceForCSE($semester_cse,$subject_code_cse,$date)
		{
			$semester = $this->validation($semester_cse);
			$subject_code = $this->validation($subject_code_cse);
			$query = "SELECT * FROM cse_student_attendance WHERE std_semester_no='$semester' AND attendance_date='$date' AND subject_code = '$subject_code' order by std_roll asc";
			$result = $this->select($query);
			$attendance = "";
			if($result){
				$attendance.= ' <section class="content">
							      <div class="row">
							      <div class="box">
							             <div class="box-header">
							            <h1 class="box-title text-center">'.date("Y-m-d").'</h1>
							            </div> 
							            <div class="box-body">
							              <table id="example1" class="table table-bordered table-striped">
							                <thead>
							                <tr>
							                  <th>Roll</th>
							                  <th>Batch No</th>
							                  <th>Semester</th>
							                  <th>Subject Code</th>
							                  <th>Attendance</th>
							                </tr>
							                </thead>
							                <tbody>';
				foreach ($result as $key => $data) {
					$attendance.='
						<tr>
                        <td>'.$data['std_roll'].'</td>
                        <td>'.$data['std_batch_no'].'</td>
                        <td>'.$data['std_semester_no'].'</td>
                        <td>'.$data['subject_code'].'</td>
                        <td>
                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
                        </td>
                    </tr>';
				}
				$attendance.=' </tbody>
				                <tfoot>
				                <tr>
				                  <th>Roll</th>
				                  <th>Batch No</th>
				                  <th>Semester</th>
				                  <th>Subject Code</th>
				                  <th>Attendance</th>
				                </tr>
				                </tfoot>
				              </table>
				            </div>
				          </div>
				      </div>
				   </section>';
			}else {
				$attendance.="<h3 class='text-center text-danger'>No Attendance Was Taken For This Subject Today</h3>";
			}
			echo $attendance;
			
		}

		public function getTodaysAttendanceForEEE($semester_eee,$subject_code_eee,$date)
		{
			$semester = $this->validation($semester_eee);
			$subject_code = $this->validation($subject_code_eee);
			$query = "SELECT * FROM eee_student_attendance WHERE std_semester_no='$semester' AND attendance_date='$date' AND subject_code = '$subject_code' order by std_roll asc";
			$result = $this->select($query);
			$attendance = "";
			if($result){
				$attendance.= ' <section class="content">
							      <div class="row">
							          
							      <div class="box">
							             <div class="box-header">
							              <h1 class="box-title text-center">'.date("Y-m-d").'</h1>
							            </div> 
							            <div class="box-body">
							              <table id="example1" class="table table-bordered table-striped">
							                <thead>
							                <tr>
							                  <th>Roll</th>
							                  <th>Batch No</th>
							                  <th>Semester</th>
							                  <th>Subject Code</th>
							                  <th>Attendance</th>
							                </tr>
							                </thead>
							                <tbody>';
				foreach ($result as $key => $data) {
					$attendance.='
						<tr>
                        <td>'.$data['std_roll'].'</td>
                        <td>'.$data['std_batch_no'].'</td>
                        <td>'.$data['std_semester_no'].'</td>
                        <td>'.$data['subject_code'].'</td>
                        <td>
                        	
                        </td>
                    </tr>';
				}
				$attendance.=' </tbody>
				                <tfoot>
				                <tr>
				                  <th>Roll</th>
				                  <th>Batch No</th>
				                  <th>Semester</th>
				                  <th>Subject Code</th>
				                  <th>Attendance</th>
				                </tr>
				                </tfoot>
				              </table>
				            </div>
				          </div>
				      </div>
				   </section>';
			}else{
				$attendance.="<h2 class='text-center text-danger'> No Attendance Taken Today For $subject_code_eee</h1>";
			}
			echo $attendance;
			
		}

		public function getTodaysAttendanceForAdministrator($select_semester,$select_department,$select_subject,$date){
			echo "kuddus";die;
			$select_semester = $this->validation($select_semester);
			$select_department = $this->validation($select_department);
			$select_subject = $this->validation($select_subject);

			if($select_department == "cse"){
				$query = "SELECT * FROM cse_student_attendance WHERE std_semester_no='$select_semester' AND subject_code='$select_subject' AND attendance_date='$date'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            <h1 class="box-title text-center">'.date("Y-m-d").'</h1>
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken Today For $select_subject</h1>";
				}
				echo $attendance;
			}elseif($select_department == "eee"){
				$query = "SELECT * FROM eee_student_attendance WHERE std_semester_no='$select_semester' AND subject_code='$select_subject' AND attendance_date='$date'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            <h1 class="box-title text-center">'.date("Y-m-d").'</h1>
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken Today For $select_subject</h1>";
				}
				echo $attendance;
			}
		}

		public function getAnyAttendanceForAdministrator($select_semester_any,$select_department_any,$select_subject_any,$select_date_any){
			$select_semester_any = $this->validation($select_semester_any);
			$select_department_any = $this->validation($select_department_any);
			$select_subject_any = $this->validation($select_subject_any);

			if($select_department_any == "cse"){
				$query = "SELECT * FROM cse_student_attendance WHERE std_semester_no='$select_semester_any' AND subject_code='$select_subject_any' AND attendance_date='$select_date_any'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            <h1 class="box-title text-center">'.date("Y-m-d").'</h1>
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken At This Day For $select_subject_any</h1>";
				}
				echo $attendance;
			}elseif($select_department_any == "eee"){
				$query = "SELECT * FROM eee_student_attendance WHERE std_semester_no='$select_semester_any' AND subject_code='$select_subject_any' AND attendance_date='$select_date_any'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            <h1 class="box-title text-center">'.date("Y-m-d").'</h1>
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken At This Day For $select_subject_any</h1>";
				}
				echo $attendance;
			}
		}


		public function getCSEDeptAnyAttendanceForTeacher($select_semester_any_for_teacher,$select_department_any_for_teacher,$select_subject_any_for_teacher,$select_date_any_for_teacher){
			$select_semester_any_for_teacher = $this->validation($select_semester_any_for_teacher);
			$select_department_any_for_teacher = $this->validation($select_department_any_for_teacher);
			$select_subject_any_for_teacher = $this->validation($select_subject_any_for_teacher);

			if($select_department_any_for_teacher == "cse"){
				$query = "SELECT * FROM cse_student_attendance WHERE std_semester_no='$select_semester_any_for_teacher' AND subject_code='$select_subject_any_for_teacher' AND attendance_date='$select_date_any_for_teacher'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            <h1 class="box-title text-center">'.date("Y-m-d").'</h1>
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken At This Day For $select_subject_any_for_teacher</h1>";
				}
				echo $attendance;
			}
		}
		public function getEEEDeptAnyAttendanceForTeacher($select_eee_semester_any_for_teacher,$select_eee_department_any_for_teacher,$select_eee_subject_any_for_teacher,$select_eee_date_any_for_teacher){
			$select_eee_semester_any_for_teacher = $this->validation($select_eee_semester_any_for_teacher);
			$select_eee_department_any_for_teacher = $this->validation($select_eee_department_any_for_teacher);
			$select_eee_subject_any_for_teacher = $this->validation($select_eee_subject_any_for_teacher);
			if($select_eee_department_any_for_teacher == "eee"){
				$query = "SELECT * FROM eee_student_attendance WHERE std_semester_no='$select_eee_semester_any_for_teacher' AND subject_code='$select_eee_subject_any_for_teacher' AND attendance_date='$select_eee_date_any_for_teacher'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken At This Day For $select_eee_subject_any_for_teacher</h1>";
				}
				echo $attendance;
			}
		}

		public function getMonthlyAttendanceForTeacher($select_semester_monthly_for_teacher,$select_department_monthly_for_teacher,$select_subject_monthly_for_teacher,$select_date_from_monthly_for_teacher,$select_date_to_monthly_for_teacher){
			$select_semester_monthly_for_teacher = $this->validation($select_semester_monthly_for_teacher);
			$select_subject_monthly_for_teacher = $this->validation($select_subject_monthly_for_teacher);



			if($select_department_monthly_for_teacher == "cse"){
				$query = "SELECT * FROM cse_student_attendance WHERE std_semester_no='$select_semester_monthly_for_teacher' AND subject_code='$select_subject_monthly_for_teacher' AND attendance_date BETWEEN '$select_date_from_monthly_for_teacher' AND '$select_date_to_monthly_for_teacher'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken Between this date</h1>";
				}
				echo $attendance;
			}
		}

		public function getEEEDeptMonthlyAttendanceForTeacher($select_eee_semester_monthly_for_teacher,$select_eee_department_monthly_for_teacher,$select_eee_subject_monthly_for_teacher,$select_eee_date_from_monthly_for_teacher,$select_eee_date_to_monthly_for_teacher){
			$select_eee_semester_monthly_for_teacher = $this->validation($select_eee_semester_monthly_for_teacher);
			$select_eee_subject_monthly_for_teacher = $this->validation($select_eee_subject_monthly_for_teacher);
			if($select_eee_department_monthly_for_teacher == "eee"){
				$query = "SELECT * FROM eee_student_attendance WHERE std_semester_no='$select_eee_semester_monthly_for_teacher' AND subject_code='$select_eee_subject_monthly_for_teacher' AND attendance_date BETWEEN '$select_eee_date_from_monthly_for_teacher' AND '$select_eee_date_to_monthly_for_teacher'";
				$result = $this->select($query);
				$attendance = "";
				if($result){
					$attendance.= ' <section class="content">
								      <div class="row">
								      <div class="box">
								             <div class="box-header">
								            </div> 
								            <div class="box-body">
								              <table id="example1" class="table table-bordered table-striped">
								                <thead>
								                <tr>
								                  <th>Roll</th>
								                  <th>Batch No</th>
								                  <th>Semester</th>
								                  <th>Subject Code</th>
								                  <th>Attendance</th>
								                </tr>
								                </thead>
								                <tbody>';
					foreach ($result as $key => $data) {
						$attendance.='
							<tr>
	                        <td>'.$data['std_roll'].'</td>
	                        <td>'.$data['std_batch_no'].'</td>
	                        <td>'.$data['std_semester_no'].'</td>
	                        <td>'.$data['subject_code'].'</td>
	                        <td>
	                        	<input type="checkbox" value="'.$data["std_attendance"].'"'.($data["std_attendance"]=='yes' ? 'checked="checked"' : '').'>'.'
	                        </td>
	                    </tr>';
					}
					$attendance.=' </tbody>
					                <tfoot>
					                <tr>
					                  <th>Roll</th>
					                  <th>Batch No</th>
					                  <th>Semester</th>
					                  <th>Subject Code</th>
					                  <th>Attendance</th>
					                </tr>
					                </tfoot>
					              </table>
					            </div>
					          </div>
					      </div>
					   </section>';
				}else{
					$attendance.="<h2 class='text-center text-danger'> No Attendance Taken Between this date</h1>";
				}
				echo $attendance;
			}
		}


}