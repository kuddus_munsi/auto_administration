<?php 
	 
	class Applier extends Database
	{
		public $student_id;

		public function dateFormat($date){
        	return   date('F j, Y ,g:i a', strtotime($date));
    	}

		public  function validation($data)
    	{
			$data = trim($data);
	        $data = stripcslashes($data);
	        $data = htmlspecialchars($data);
	        return $data;
		}
		
		public function getAutoFormNumber()
		{
			$sqlQuery = "SELECT form_no FROM persons order by form_no desc limit 1";
			$result = $this->select($sqlQuery);
			foreach($result as $number){
				$value = $number['form_no'];
			}
			return  (int)$value;
			

		}

		public function storeInfo($data)
		{
			
			$form_number =$this->validation($data['form_number']);
			$admission_department =$this->validation( $data['select_department']);
			$student_name = $this->validation($data['student_name']);
			$student_email = $this->validation($data['student_email']);
			$student_phone = $this->validation($data['student_phone']);
			$father_name = $this->validation($data['father_name']);
			$mother_name = $this->validation($data['mother_name']);
			$gardians_name = $this->validation($data['gardians_name']);
			$rel_with_gardian = $this->validation($data['rel_with_gardian']);
			$present_address = $this->validation($data['present_address']);
			$permanent_address = $this->validation($data['permanent_address']);
			$date_of_birth = $this->validation($data['date_of_birth']);
			$nationality = $this->validation($data['nationality']);
			$religion = $this->validation($data['religion']);
			//ssc result
			$ssc_examination = $this->validation($data['ssc_examination']);
			$ssc_board = $this->validation($data['ssc_board']);
			$ssc_year = $this->validation($data['ssc_year']);
			$ssc_school_college = $this->validation($data['ssc_school_college']);
			$ssc_divition_grade = $this->validation($data['ssc_divition_grade']);
			$ssc_cgpa =$this->validation( $data['ssc_cgpa']);
			$ssc_subject = $this->validation($data['ssc_subject']);
			// hsc result
			$hsc_examination = $this->validation($data['hsc_examination']);
			$hsc_board = $this->validation($data['hsc_board']);
			$hsc_year = $this->validation($data['hsc_year']);
			$hsc_school_college = $this->validation($data['hsc_school_college']);
			$hsc_divition_grade =$this->validation( $data['hsc_divition_grade']);
			$hsc_cgpa = $this->validation($data['hsc_cgpa']);
			$hsc_subject = $this->validation($data['hsc_subject']);
			$break_of_study = $this->validation($data['hsc_board']);

			$permitted = array('jpg' , 'jpeg' , 'png');
			$file_Name = $_FILES['student_image']['name'];
			$file_Size = $_FILES['student_image']['size'];
			$file_Temp = $_FILES['student_image']['tmp_name'];
			$div = explode('.',$file_Name);
			$file_ext = strtolower(end($div));
			$unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
			$uploaded_image = "student_image/".$unique_image;
			move_uploaded_file($file_Temp ,$uploaded_image);





			if($student_name=="" OR $admission_department == ""  OR $father_name =="" OR $mother_name =="" OR $gardians_name =="" OR $rel_with_gardian =="" OR $present_address =="" OR $permanent_address =="" OR $date_of_birth=="" OR $nationality =="" OR $religion =="" OR $ssc_examination =="" OR $ssc_board =="" OR $ssc_year =="" OR $ssc_school_college=="" OR $ssc_divition_grade =="" OR $ssc_cgpa =="" OR $ssc_subject=="" OR $hsc_examination =="" OR $hsc_board =="" OR $hsc_year =="" OR $hsc_school_college=="" OR $hsc_divition_grade =="" OR $hsc_cgpa =="" OR $hsc_subject=="" OR $file_Name=="")
			{
				 Message::showMessage("Input can not be empty");
			}elseif(!filter_var($student_email,FILTER_VALIDATE_EMAIL)){
				 Message::showMessage("Invalid email!! Try with a valid email");
			}elseif($file_Size > 3000000){
				Message::showMessage("Image size shoul be less than 300kb");
			}
			elseif (in_array($file_ext , $permitted) === false){
				Message::showMessage("Error!! you can upload only :-".implode(',' , $permitted));
			}else{
				$sqlQuery = "INSERT INTO persons(form_no,p_name,p_email,p_adm_dept,p_phone,p_image)VALUES('$form_number','$student_name','$student_email','$admission_department','$student_phone','$uploaded_image')";
  				$result = $this->insert($sqlQuery);

		        if($result){
            		$sqlQuery = "INSERT INTO persons_details (form_no,p_father_name,p_mother_name,p_guardian_name,p_guard_relation,p_per_address,p_pre_address,p_nationality,p_religion,p_dob,p_study_gap) VALUES ('$form_number','$father_name','$mother_name','$gardians_name','$rel_with_gardian','$permanent_address','$present_address','$nationality','$religion','$date_of_birth','$break_of_study');";
		        $success= $this->insert($sqlQuery);
		        	if($success){
		        		$sqlQuery = "INSERT INTO persons_academic_qualification (form_no,p_ac_ssc_exam,p_ac_ssc_board,p_ac_ssc_year,p_ac_college_ssc_school,p_ac_div_ssc_grade,p_ac_ssc_cgpa,p_ac_ssc_subject,p_ac_hsc_exam,p_ac_hsc_board,p_ac_hsc_year,p_ac_college_hsc_school,p_ac_div_hsc_grade,p_ac_hsc_cgpa,p_ac_hsc_subject) VALUES ('$form_number','$ssc_examination','$ssc_board','$ssc_year','$ssc_school_college','$ssc_divition_grade','$ssc_cgpa','$ssc_subject','$hsc_examination','$hsc_board','$hsc_year','$hsc_school_college','$hsc_divition_grade','$hsc_cgpa','$hsc_subject');";
                        $done=$this->insert($sqlQuery);
		        		if($done){
		        			Message::showMessage("Success!! All data inserted successfully");
		        		}else{
		        			Message::showMessage("Error! Data has been not inserted");
		        		}
		        	}else{
		        		Message::showMessage("Error! Qualification data has not inserted ");
		        	}
        		}else{
            		Message::showMessage("Error! Details data has been not inserted");
        		}


			}
		}

		public function show()
		{
			$sqlQuery = "SELECT * FROM persons order by form_no desc ";
			$applierInfo = $this->select($sqlQuery);
			return $applierInfo;
		}

		public function showSingle($form_no)
		{
			$sqlQuery = "SELECT persons.*, persons_details.*, persons_academic_qualification.* from persons left join persons_details on persons.form_no = persons_details.form_no left join persons_academic_qualification on persons_academic_qualification.form_no = persons.form_no where persons.form_no = '$form_no'";
			$applierSingleInfo = $this->select($sqlQuery);
			return $applierSingleInfo;
		}


	}