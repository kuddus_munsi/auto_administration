<?php 
    include "source/header.php" ;
    include "source/sidebar.php" ;
 

    $teacher = new Teacher();


    if(isset($_GET['teacherDel_code'])){
      $teacher_code = $_GET['teacherDel_code'];
      $teacher->deleteTeacher($teacher_code);
    }

    if($_SERVER['REQUEST_METHOD'] == "POST"){
      $update = $teacher->updateTeacherInfo($_POST);
    }
?>



  <div class="content-wrapper">
    <section class="content-header">
      <h1>
      List of All Teachers
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">teacher</li>
      </ol>
    </section>
<?php
  if(isset($_GET['teacherEdit_code'])){
      $teacher_code = $_GET['teacherEdit_code'];
      $getSingleTeacher = $teacher->getSingleTeacerInfo($teacher_code);
      if($getSingleTeacher){
        foreach ($getSingleTeacher as $info) {
    ?>
  <section class="content">
   <div class="row">
    <div class="box box-primary">
          <div class="box-body">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <form role="form" method="post" action="teacher-list.php" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="teacher_name">Teacher Name</label>
                  <input type="text" class="form-control" id="teacher_name" name="teacher_name" value="<?php echo $info['teacher_name'];?>">
                </div>

                <div class="form-group">
                  <label for="teacher_code">Teacher ID</label>
                  <input type="text" class="form-control" id="teacher_code" name="teacher_code" value="<?php echo $info['teacher_code'];?>">
                </div>

                <div class="form-group">
                  <label for="teacher_designation">Teacher Designation</label>
                  <input type="text" class="form-control" id="teacher_designation" name="teacher_designation" value="<?php echo $info['teacher_designation'];?>">
                </div>


                <div class="form-group">
                  <label for="teacher_department">Teacher Department</label>
                  <select  class="form-control" id="teacher_department" name="teacher_department">
                    <option value="cse">CSE</option>
                    <option value="eee">EEE</option>
                    <option value="ete">ETE</option>
                  </select>
                </div>

                  <div class="form-group">
                    <label for="teacher_image">Teacher Image</label>
                    <input type="file" class="form-control" id="teacher_image" name="teacher_image">
                </div>
                <h5 style="font-weight: bold;">Pevious Image</h5>
                  <img src="<?php echo $info['teacher_image']; ?>" height="80px" weight="90px"><br><br>
                    <input type="hidden" name="teacher_previous_code" value="<?php echo $info['teacher_code'];?>">
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <div class="col-md-2"></div>
          </div>
        </div>
        </div>
        </section>
<?php } } } ?>

    <section class="content">
      <div class="row">
        <div class="box box-primary">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Serial</th>
                  <th>Teacher Name</th>
                  <th>Teaher Code</th>
                  <th>Teacher Designation</th>
                  <th>Teacher Department</th>
                  <th>Teacher Imgae</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $showList = $teacher->getAllTeacherList();
                    if($showList){
                        foreach($showList as $key=>$data){$key+=1;?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $data['teacher_name']; ?></td>
                        <td><?php echo $data['teacher_code']; ?></td>
                        <td><?php echo $data['teacher_designation']; ?></td>
                        <td><?php echo $data['teacher_department']; ?></td>
                        <td><img src="<?php echo $data['teacher_image']; ?>" height="80px" weight="90px"></td>
                        <td>
                          <a href="?teacherEdit_code=<?php echo $data['teacher_code'];?>">Edit</a> 
                          ||
                          <a onclick="return confirm('Are sure to delete??');" href="?teacherDel_code=<?php echo $data['teacher_code'];?>">Delete</a>
                        </td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Serial</th>
                  <th>Teacher Name</th>
                  <th>Teaher Code</th>
                  <th>Teacher Designation</th>
                  <th>Teacher Department</th>
                  <th>Teacher Image</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>


      </div>
   </section>
<?php include "source/footer.php" ; ?>