<?php 
	
	require_once __DIR__.'/../vendor/autoload.php';

  	 $attendance = new Attendance();

	 if($_SERVER['REQUEST_METHOD'] == "POST" AND isset($_POST['dept']) AND isset($_POST['semester'])){
      $dept = $_POST['dept'];
      $semester = $_POST['semester'];
      $attendance->chekcAttendancePermissionStatus($dept,$semester);
   }



   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['semester_cse']) && isset($_POST['subject_code_cse']) && isset($_POST['request_open_action_cse'])){
    $semester = $_POST['semester_cse'];
    $subject_code = $_POST['subject_code_cse'];
    $attendance->sendAttendanceRequestForCSE($semester,$subject_code);
   }

   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['semester_cse']) && isset($_POST['subject_code_cse']) && isset($_POST["request_close_action_cse"])){
    $semester = $_POST['semester_cse'];
    $subject_code = $_POST['subject_code_cse'];
    $attendance->closeAttendanceRequestForEEE($semester,$subject_code);
   }



/*todays cse atttendance for teacher*/
   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['semester_cse']) && isset($_POST['subject_code_cse']) && isset($_POST["todayatt_cse"])){

      $semester_cse = $_POST['semester_cse'];
      $subject_code_cse = $_POST['subject_code_cse'];
      $date= date("Y-m-d");
      $attendance->getTodaysAttendanceForCSE($semester_cse,$subject_code_cse,$date);
   }
/*//todays cse atttendance for teacher*/



/*show any attendance for teacher*/
   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_semester_any_for_teacher']) && isset($_POST['select_department_any_for_teacher']) && isset($_POST['select_subject_any_for_teacher'])  && isset($_POST['select_date_any_for_teacher'])){

      $select_semester_any_for_teacher = $_POST['select_semester_any_for_teacher'];
      $select_department_any_for_teacher = $_POST['select_department_any_for_teacher'];
      $select_subject_any_for_teacher = $_POST['select_subject_any_for_teacher'];
      $select_date_any_for_teacher = $_POST['select_date_any_for_teacher'];
      $attendance->getCSEDeptAnyAttendanceForTeacher($select_semester_any_for_teacher,$select_department_any_for_teacher,$select_subject_any_for_teacher,$select_date_any_for_teacher);
   }
/*//show any attendance for teacher*/

/*monthly attendance for teacher*/

   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_semester_monthly_for_teacher']) && isset($_POST['select_department_monthly_for_teacher']) && isset($_POST['select_subject_monthly_for_teacher'])  && isset($_POST['select_date_from_monthly_for_teacher']) && isset($_POST['select_date_to_monthly_for_teacher'])){

    $select_semester_monthly_for_teacher = $_POST['select_semester_monthly_for_teacher'];
    $select_department_monthly_for_teacher = $_POST['select_department_monthly_for_teacher'];
    $select_subject_monthly_for_teacher = $_POST['select_subject_monthly_for_teacher'];
    $select_date_from_monthly_for_teacher = $_POST['select_date_from_monthly_for_teacher'];
    $select_date_to_monthly_for_teacher = $_POST['select_date_to_monthly_for_teacher'];
      $attendance->getMonthlyAttendanceForTeacher($select_semester_monthly_for_teacher,$select_department_monthly_for_teacher,$select_subject_monthly_for_teacher,$select_date_from_monthly_for_teacher,$select_date_to_monthly_for_teacher);
   }

/*//monthly attendance for teacher*/

//subject load for any day attendance
   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_semester_any_for_teacher']) && isset($_POST['select_department_any_for_teacher']) && isset($_POST["any_day_att_sub"])){

      $select_semester_any_for_teacher = $_POST['select_semester_any_for_teacher'];
      $select_department_any_for_teacher = $_POST['select_department_any_for_teacher'];

      if($select_department_any_for_teacher == "cse"){
         $attendance->showCourseBySemesterForCSEAttendance($select_semester_any_for_teacher);
      }
      
   }

///subject load for monthly attendance
   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_semester_monthly_for_teacher']) && isset($_POST['select_department_monthly_for_teacher']) && isset($_POST["monthly_att_sub"])){

      $select_semester_monthly_for_teacher = $_POST['select_semester_monthly_for_teacher'];
      $select_department_monthly_for_teacher = $_POST['select_department_monthly_for_teacher'];

      if($select_department_monthly_for_teacher == "cse"){
         $attendance->showCourseBySemesterForCSEAttendance($select_semester_monthly_for_teacher);
      }
      
   }


 ?>