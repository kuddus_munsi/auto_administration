<?php 
	
   require_once __DIR__.'/../vendor/autoload.php';

   $attendance = new Attendance();



  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['semester_eee']) && isset($_POST['subject_code_eee']) && isset($_POST["open_action_eee"])){
   	$semester = $_POST['semester_eee'];
   	$subject_code = $_POST['subject_code_eee'];
   	$attendance->sendAttendanceRequestForEEE($semester,$subject_code);
   }


  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['semester_eee']) && isset($_POST['subject_code_eee']) && isset($_POST["close_action_eee"])){
   	$semester = $_POST['semester_eee'];
   	$subject_code = $_POST['subject_code_eee'];
   	$attendance->closeAttendanceRequestForEEE($semester,$subject_code);
   }



/*todays eee atttendance for teacher*/

   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['semester_eee']) && isset($_POST['subject_code_eee']) && isset($_POST['todayatt_eee'])){

      $semester_eee = $_POST['semester_eee'];
      $subject_code_eee = $_POST['subject_code_eee'];
      $date= date("Y-m-d");
      $attendance->getTodaysAttendanceForEEE($semester_eee,$subject_code_eee,$date);
   }
/*//todays eee atttendance for teacher*/




/*show any attendance for teacher*/
   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_eee_semester_any_for_teacher']) && isset($_POST['select_eee_department_any_for_teacher']) && isset($_POST['select_eee_subject_any_for_teacher'])  && isset($_POST['select_eee_date_any_for_teacher'])){

      $select_eee_semester_any_for_teacher = $_POST['select_eee_semester_any_for_teacher'];
      $select_eee_department_any_for_teacher = $_POST['select_eee_department_any_for_teacher'];
      $select_eee_subject_any_for_teacher = $_POST['select_eee_subject_any_for_teacher'];
      $select_eee_date_any_for_teacher = $_POST['select_eee_date_any_for_teacher'];
      $attendance->getEEEDeptAnyAttendanceForTeacher($select_eee_semester_any_for_teacher,$select_eee_department_any_for_teacher,$select_eee_subject_any_for_teacher,$select_eee_date_any_for_teacher);
   }
/*//show any attendance for teacher*/

/*monthly attendance for teacher*/

   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_eee_semester_monthly_for_teacher']) && isset($_POST['select_eee_department_monthly_for_teacher']) && isset($_POST['select_eee_subject_monthly_for_teacher'])  && isset($_POST['select_eee_date_from_monthly_for_teacher']) && isset($_POST['select_eee_date_to_monthly_for_teacher'])){

    $select_eee_semester_monthly_for_teacher = $_POST['select_eee_semester_monthly_for_teacher'];
    $select_eee_department_monthly_for_teacher = $_POST['select_eee_department_monthly_for_teacher'];
    $select_eee_subject_monthly_for_teacher = $_POST['select_eee_subject_monthly_for_teacher'];
    $select_eee_date_from_monthly_for_teacher = $_POST['select_eee_date_from_monthly_for_teacher'];
    $select_eee_date_to_monthly_for_teacher = $_POST['select_eee_date_to_monthly_for_teacher'];

      $attendance->getEEEDeptMonthlyAttendanceForTeacher($select_eee_semester_monthly_for_teacher,$select_eee_department_monthly_for_teacher,$select_eee_subject_monthly_for_teacher,$select_eee_date_from_monthly_for_teacher,$select_eee_date_to_monthly_for_teacher);
   }

/*//monthly attendance for teacher*/




//subject load for any day attendance
   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_eee_semester_any_for_teacher']) && isset($_POST['select_eee_department_any_for_teacher']) && isset($_POST['subject_for_anyday_eee'])){

      $select_eee_semester_any_for_teacher = $_POST['select_eee_semester_any_for_teacher'];
      $select_eee_department_any_for_teacher = $_POST['select_eee_department_any_for_teacher'];
	 if($select_eee_department_any_for_teacher == "eee"){
         $attendance->showCourseBySemesterForEEEAttendance($select_eee_semester_any_for_teacher);
      }
      
   }


///subject load for monthly attendance
   if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['select_eee_semester_monthly_for_teacher']) && isset($_POST['select_eee_department_monthly_for_teacher']) && isset($_POST["subject_for_month_eee"])){

      $select_eee_semester_monthly_for_teacher = $_POST['select_eee_semester_monthly_for_teacher'];
      $select_eee_department_monthly_for_teacher = $_POST['select_eee_department_monthly_for_teacher'];
	  if($select_eee_department_monthly_for_teacher == "eee"){
         $attendance->showCourseBySemesterForEEEAttendance($select_eee_semester_monthly_for_teacher);
      }
      
   }

 ?>