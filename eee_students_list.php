<?php 
    include "source/header.php" ;
    include "source/sidebar.php" ;
    
    $student = new Students();
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      List of All EEE Studnts
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Studnts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
<script>

</script>

          
      <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title"></h3>
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Serial</th>
                  <th>Student Roll</th>
                  <th>Student Name</th>
                  <th>Student Email</th>
                  <th>Phone</th>
                  <th>Department</th>
                  <th>Batch No</th>
                  <th>Session</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $showList = $student->showEEEStudents();
                    if($showList){
                        foreach($showList as $key=>$data){$key+=1;?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td><?php echo $data['std_roll_no']; ?></td>
                        <td><?php echo $data['std_name']; ?></td>
                        <td><?php echo $data['std_email']; ?></td>
                        <td><?php echo $data['std_phone']; ?></td>
                        <td><?php echo $data['std_dept_name']; ?></td>
                        <td><?php echo $data['std_batch_no']."th"; ?></td>
                        <td><?php echo $student->dateFormat($data['std_session_from'])."<br> <span class='text-danger  text-bold'>to</span><br> ".$student->dateFormat($data['std_session_to']); ?></td>
                        <td><a href="view_eee_student_details.php?std_roll_no=<?php echo $data['std_roll_no'];?>">View Details</a></td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Serial</th>
                  <th>Student Roll</th>
                  <th>Student Name</th>
                  <th>Student Email</th>
                  <th>Phone</th>
                  <th>Department</th>
                  <th>Batch No</th>
                  <th>Session</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>


      </div>
   </section>
<?php include "source/footer.php" ; ?>