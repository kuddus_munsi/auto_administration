<?php 
    include "source/header.php" ;
    include "source/sidebar.php" ;
    
    $batch = new Batches();
    $msg = Message::getMessage();

    if(isset($_GET['batch_id'])){
        $id = $_GET['batch_id'];
        $delete = $batch->delete($id);
    }
?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
      List of All Batch no 
      <h2 class="text-center text-success"><?php echo "<div id='message'> $msg</div>"?> </h2>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">new applier</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
<script>

</script>

          
      <div class="box">
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Batch No</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                    $show_batch = $batch->show();
                    if($show_batch){
                        foreach($show_batch as $serial=>$no){?>
                    <tr>
                        <td><?php echo $serial+1; ?></td>
                        <td><?php echo $no['batch_no'] ; ?></td>
                        <td><a href="?batch_id=<?php echo $no['id'];?>">Delete</a></td>
                    </tr>
                <?php  } } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Form No</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>


      </div>
   </section>
<?php include "source/footer.php" ; ?>