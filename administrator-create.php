<?php 
  include "source/header.php" ;
  include "source/sidebar.php" ;

  $administrator = new Administrator();

  if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST)){
    $administrator->createNewAdministrator($_POST);
 
  }
  $msg = Message::getMessage();


 ?>

 <script type="text/javascript">
    $(document).ready(function() {
        $("#submit").click(function(){
            $("#administrator_create_form").ajaxForm().submit();
        });
    }); 
</script>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
     <h2 class="text-center text-danger"><?php echo "<div id='message'> $msg</div>"?> </h2>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
 <section class="content">
      <div class="row">
         <div class="box box-primary">
          <div class="box-body">
            <h2 class="text-center">Create Administrator</h2>
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <form role="form" method="post" action="" enctype="multipart/form-data" id="administrator_create_form">
              <div class="box-body">
                <div class="form-group">
                  <label for="administrator_name">Administrator Name</label>
                  <input type="text" class="form-control" id="administrator_name" name="administrator_name" placeholder="Administrator name ">
                </div>

                <div class="form-group">
                  <label for="administrator_code">Administrator ID</label>
                  <input type="text" class="form-control" id="administrator_code" name="administrator_code" placeholder="Administrator code ">
                </div>

                <div class="form-group">
                  <label for="administrator_role">Administrator Role</label>
                  <select  class="form-control" id="administrator_role" name="administrator_role">
                    <option value="super_admin">Super Admin</option>
                    <option value="admin">Admin</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="administrator_password">Administrator Password</label>
                  <input type="text" class="form-control" id="administrator_password" name="administrator_password" placeholder="Password">
                </div>

                <div class="form-group">
                  <label for="administrator_image">Teacher Image</label>
                  <input type="file" class="form-control" id="administrator_image" name="administrator_image">
                </div>


                <div class="form-group">
                  <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>

                </div>
              </form>
            </div>
            <div class="col-md-2"></div>
          </div>
        </div>


      </div>
   </section>
<?php include "source/footer.php" ; ?>